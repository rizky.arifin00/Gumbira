package com.gumbira.application.gumbiratour.view.ui;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;


import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.view.animation.RevealBackgroundView;
import com.gumbira.application.gumbiratour.databinding.ActivitySearchBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.adapter.ListSearchAdapter;
import com.gumbira.application.gumbiratour.viewmodel.PlacesViewModel;

import java.util.ArrayList;
import java.util.List;


public class SearchActivity extends AppCompatActivity implements RevealBackgroundView.OnStateChangeListener {
    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";

    private ActivitySearchBinding binding;
    private ListSearchAdapter listSearchAdapter;
    private PlacesViewModel placesViewModel;
    long delay = 1000; // 1 seconds after user stop
    // s typing
    long last_text_edit = 0;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        binding.searchToolbar.setTitle("");
        setSupportActionBar(binding.searchToolbar);
        listSearchAdapter = new ListSearchAdapter();
        binding.expsearchRvSearch.setLayoutManager(new LinearLayoutManager(this));
        binding.expsearchRvSearch.setAdapter(listSearchAdapter);

        placesViewModel = ViewModelProviders.of(this).get(PlacesViewModel.class);

        setupRevealBackground(savedInstanceState);

        binding.expsearchEtSearch.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        handler.removeCallbacks(input_finish_checker);
                        if (count >= 1) {
                            binding.expsearchTvReset.setVisibility(View.VISIBLE);
                        } else {
                            binding.expsearchTvReset.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void afterTextChanged(final Editable s) {
                        if (s.length() > 0) {
                            last_text_edit = System.currentTimeMillis();
                            handler.postDelayed(input_finish_checker, delay);
                        } else {

                        }
                    }
                }

        );
        binding.expsearchTvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Clear the second EditText
                clearList();
                binding.expsearchEtSearch.getText().clear();
                binding.searchnotfound.setVisibility(View.GONE);
            }
        });


    }

    private void setupRevealBackground(Bundle savedInstanceState) {
        binding.vRevealBackground.setOnStateChangeListener(this);
        if (savedInstanceState == null) {
            final int[] startingLocation = getIntent().getIntArrayExtra(ARG_REVEAL_START_LOCATION);
            binding.vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    binding.vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    binding.vRevealBackground.startFromLocation(startingLocation);
                    return true;
                }
            });
        } else {
            binding.vRevealBackground.setToFinishedFrame();
        }
    }

    @Override
    public void onStateChange(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            binding.contentSearch.setVisibility(View.VISIBLE);
            binding.searchAppbar.setVisibility(View.VISIBLE);
        } else {
            binding.contentSearch.setVisibility(View.INVISIBLE);
            binding.searchAppbar.setVisibility(View.INVISIBLE);
        }
    }

    public static void startActivity(int[] startingLocation, Activity startingActivity){
        Intent intent = new Intent(startingActivity, SearchActivity.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        startingActivity.startActivity(intent);
    }

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                observeViewModel(placesViewModel, binding.expsearchEtSearch.getText().toString());
            }
        }
    };

    private void clearList() {
        List<Result> results = new ArrayList<>();
        listSearchAdapter.setListWisata(results);

    }

    public void back(View view) {
        onBackPressed();
    }

    private void observeViewModel(PlacesViewModel placesViewModel, String text) {
        placesViewModel.setKey(text,"");
        placesViewModel.getProjectListObservable().observe(this, new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> results) {
                if (results != null) {
                    listSearchAdapter.setListWisata(results);
                    if (results.size() == 0) {
                        binding.expsearchRvSearch.setVisibility(View.GONE);
                        binding.searchnotfound.setVisibility(View.VISIBLE);
                    }
                    binding.expsearchRvSearch.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back_search:
                onBackPressed();
                break;
        }
        return false;
    }

}
