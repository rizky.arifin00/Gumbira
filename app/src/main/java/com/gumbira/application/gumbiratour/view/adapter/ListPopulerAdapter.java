package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemPopulerBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.callback.PlaceClickCallback;
import com.gumbira.application.gumbiratour.view.ui.DetailPlacesActivity;

import java.util.List;

public class ListPopulerAdapter extends RecyclerView.Adapter<ListPopulerAdapter.PopulerViewHolder> {
    List<? extends Result> listPopuler;
    private Context c;
    @Nullable
    private final PlaceClickCallback placeClickCallback;

    public ListPopulerAdapter(@Nullable PlaceClickCallback placeClickCallback) {
        this.placeClickCallback = placeClickCallback;
    }

    public void setListPopuler(final List<? extends Result> listPopuler) {
        this.listPopuler = listPopuler;
        System.out.println(listPopuler.size());
        notifyDataSetChanged();
    }

    @Override
    public PopulerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPopulerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_populer,parent,false);
        c = parent.getContext();
        return new PopulerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PopulerViewHolder holder, final int position) {
        holder.binding.setPopular(listPopuler.get(position));
//        holder.binding.setCoba(placeClickCallback);
        holder.binding.itemPopuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPlacesActivity.class);
                i.putExtra("id", listPopuler.get(position).getPlaceId());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listPopuler != null) {
            if (listPopuler.size() < 5){
                return listPopuler.size();
            }
            else {
                return 3;
            }
        }
        else {
            return 0;
        }
    }

    static class PopulerViewHolder extends RecyclerView.ViewHolder{
        final ItemPopulerBinding binding;

        public PopulerViewHolder(ItemPopulerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
