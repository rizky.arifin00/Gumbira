package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.gumbira.application.gumbiratour.service.model.gmaps.Example;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.service.repository.MapsRepository;

import java.util.List;

/**
 * Created by rizky on 28/10/17.
 */

public class PlacesViewModel extends AndroidViewModel {
    private LiveData<List<Result>> projectListObservable;
    private LiveData<Example> projectList;

    public PlacesViewModel(Application application) {
        super(application);
    }

    /**
     * Expose the LiveData Projects query so the UI can observe it.
     */
    public LiveData<List<Result>> getProjectListObservable() {
        return projectListObservable;
    }

    public void setKey(String query, String type){
        projectListObservable = MapsRepository.getInstance().getPlaces(query, type);
    }

}
