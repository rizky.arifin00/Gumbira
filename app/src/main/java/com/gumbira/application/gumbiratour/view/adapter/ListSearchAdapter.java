package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.gumbira.application.gumbiratour.databinding.ItemSearchBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.view.ui.DetailPlacesActivity;

import java.util.List;

/**
 * Created by dery on 11/10/17.
 */

public class ListSearchAdapter extends RecyclerView.Adapter<ListSearchAdapter.viewHolder> {
    List<? extends Result> listWisata;
    private Context c;

    public void setListWisata(final List<? extends Result> listWisata) {
        this.listWisata = listWisata;
        notifyDataSetChanged();
        System.out.println(listWisata.size());
    }


    @Override
    public ListSearchAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSearchBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_search, parent, false);
        c = parent.getContext();
        return new viewHolder(binding);

    }

    @Override
    public void onBindViewHolder(ListSearchAdapter.viewHolder holder, final int position) {
        ItemSearchBinding binding = holder.binding;
        binding.setSearch(listWisata.get(position));
        binding.itemCardSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPlacesActivity.class);
                i.putExtra("id", listWisata.get(position).getPlaceId());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount(){
        if(listWisata != null) {
            System.out.println(listWisata.size());
            return listWisata.size();
        }
        else {
            return 0;
        }
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        private ItemSearchBinding binding;

        public viewHolder(ItemSearchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }
}
/*//    @Override
//    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(c).inflate(R.layout.item_search, parent, false);
//        return new ListSearchAdapter.viewHolder(v);
//    }
//
//    @Override
//    public void onBindViewHolder(final ListExploreAdapter.viewHolder holder, int position) {
//        (searchList != null) {
//            search = searchList.get(position);
//            (searchList.get(position) != null) {
//                holder.namaitem.setText(search.getNamaitem());
//                holder.kategori.setText(search.getKategori());
//                holder.alamat.setText(search.getAlamat());
//            }
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        (searchList != null) {
//            return searchList.size();
//        }
//
//        class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
//            TextView namaitem, alamat, kategori;
//            RelativeLayout itemsearch;
//
//            public viewHolder(View itemView) {
//                super(itemView);
//
//                itemsearch = (RelativeLayout) itemView.findViewById(R.id.itemsearch);
//                namaitem = (TextView) itemView.findViewById(R.id.nama_item);
//                alamat = (TextView) itemView.findViewById(R.id.alamat);
//                kategori = (TextView) itemView.findViewById(R.id.kategori);
//            }
//        }
//    }*/

