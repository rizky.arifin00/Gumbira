package com.gumbira.application.gumbiratour.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityWantToGoBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.WantogoModel;
import com.gumbira.application.gumbiratour.view.adapter.ListWantogoAdapter;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.viewmodel.WanToGoViewModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class WantToGoActivity extends AppCompatActivity {
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mRef = mRootRef.child("wishlist");
    private ListWantogoAdapter listWantogoAdapter;
    List<WantogoModel> wantogoList;
    WantogoModel wantToGo;
    private WanToGoViewModel wanToGoViewModel;

    private ActivityWantToGoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_want_to_go);
        wantogoList = new ArrayList<>();

        binding.wtgToolbar.setTitle("Want to go");
        setSupportActionBar(binding.wtgToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        binding.wtgListWtg.setLayoutManager(new LinearLayoutManager(this));
        listWantogoAdapter = new ListWantogoAdapter();
        binding.wtgListWtg.setAdapter(listWantogoAdapter);

        WanToGoViewModel.Factory factory = new WanToGoViewModel.Factory(this.getApplication(), UserShareData.getInstance(this).getValue().getUid());
        wanToGoViewModel = ViewModelProviders.of(this, factory).get(WanToGoViewModel.class);

        observeViewModel(wanToGoViewModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.home_wantogo, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    private void observeViewModel(WanToGoViewModel wanToGoViewModel){
        wanToGoViewModel.getListWantogo().observe(this, new Observer<List<WantogoModel>>() {
            @Override
            public void onChanged(@Nullable List<WantogoModel> wantogoModels) {
                listWantogoAdapter.setListPlace(wantogoModels);
            }
        });
    }
}
