package com.gumbira.application.gumbiratour.view.sharedata;

import android.content.Context;
import android.content.SharedPreferences;

import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.google.gson.Gson;

/**
 * Created by rizky on 04/11/17.
 */

public class UserShareData {
    private static UserShareData userShareData;
    private UserModel userModel;
    private Gson gson;
    private String json;
    private static final String PREFS_NAME = "User";
    private static final String USERDATA_KEY = "userdata";
    private SharedPreferences.Editor editor;
    private SharedPreferences settings;

    private UserShareData(Context context) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        userModel = new UserModel();
        gson = new Gson();
    }

    public synchronized static UserShareData getInstance(Context c) {
        if (userShareData == null) {
            if (userShareData == null) {
                userShareData = new UserShareData(c);
            }
        }
        return userShareData;
    }

    public void save(UserModel userModel) {
        json = gson.toJson(userModel);
        editor.putString(USERDATA_KEY, json);
        editor.commit();
    }

    public UserModel getValue() {
        json = settings.getString(USERDATA_KEY, "");
        return gson.fromJson(json, UserModel.class);
    }

    public void clearUserShareData() {
        editor.clear();
        editor.commit();
    }

    public void removeValue() {
        editor.remove(USERDATA_KEY);
        editor.commit();
    }
}
