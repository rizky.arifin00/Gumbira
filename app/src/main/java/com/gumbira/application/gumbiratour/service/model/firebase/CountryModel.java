package com.gumbira.application.gumbiratour.service.model.firebase;

import java.util.List;

/**
 * Created by rizky on 08/12/17.
 */

public class CountryModel {
    private String id_country;
    private String country;
    private List<String> city;
    private String photo;

    public CountryModel(){}

    public CountryModel(String id_country, String country, List<String> city) {
        this.id_country = id_country;
        this.country = country;
        this.city = city;
    }

    public String getId_country() {
        return id_country;
    }

    public void setId_country(String id_country) {
        this.id_country = id_country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
