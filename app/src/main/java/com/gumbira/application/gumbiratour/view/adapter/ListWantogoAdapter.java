package com.gumbira.application.gumbiratour.view.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemWantToGoBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.WantogoModel;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.view.ui.DetailPlacesActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by dery on 12/10/17.
 */

public class ListWantogoAdapter extends RecyclerView.Adapter<ListWantogoAdapter.viewHolder> {
    List<? extends WantogoModel> listWtg;
    private Context c;

    public void setListPlace(final List<? extends WantogoModel> listWtg) {
        this.listWtg = listWtg;
        System.out.println(listWtg.size());
        notifyDataSetChanged();
    }

    @Override
    public ListWantogoAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemWantToGoBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_want_to_go, parent, false);
        c = parent.getContext();
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, final int position) {
        ItemWantToGoBinding binding = holder.binding;
        binding.setListWtg(listWtg.get(position));
        binding.listWtgCvItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder
                        .setMessage("Are you sure to delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                mRootRef.child("wishlist").child(UserShareData.getInstance(c).getValue().getUid()).
                                child(listWtg.get(position).getWishid()).removeValue();
                                System.out.println(listWtg.get(position).getWishid());
                                Toast.makeText(c, "Selected item was deleted !", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return false;
            }
        });

        binding.listWtgCvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPlacesActivity.class);
                i.putExtra("id", listWtg.get(position).getPlaceid());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listWtg != null) {
            return listWtg.size();
        } else {
            return 0;
        }
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemWantToGoBinding binding;

        public viewHolder(ItemWantToGoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}


