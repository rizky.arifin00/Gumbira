package com.gumbira.application.gumbiratour.view.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityItenaryBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.ItenaryModel;
import com.gumbira.application.gumbiratour.view.adapter.ListItenaryAdapter;

import java.util.ArrayList;

public class ItenaryActivity extends AppCompatActivity {
    private ListItenaryAdapter adapter;
    private ActivityItenaryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_itenary);

        binding.itenaryToolbar.setTitle("Itenary");
        setSupportActionBar(binding.itenaryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        Bundle b = getIntent().getExtras();
        adapter = new ListItenaryAdapter();
        binding.expandableListView.setAdapter(adapter);
        binding.expandableListView.setGroupIndicator(null);

        if (b!=null) {
            adapter.setTourPackList((ArrayList<ItenaryModel>) b.getSerializable("itenary"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
}
