package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.gumbira.application.gumbiratour.service.model.firebase.WantogoModel;
import com.gumbira.application.gumbiratour.service.repository.FirebaseRepository;

import java.util.List;

/**
 * Created by rizky on 13/12/17.
 */

public class WanToGoViewModel extends AndroidViewModel {
    private LiveData<List<WantogoModel>> listWantogo;

    public WanToGoViewModel(Application application, final String userid) {
        super(application);
        listWantogo = FirebaseRepository.getInstance().getWantToGo(userid);
    }

    public LiveData<List<WantogoModel>> getListWantogo(){return listWantogo;}

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final String userid;

        public Factory(@NonNull Application application, String userid) {
            this.application = application;
            this.userid = userid;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new WanToGoViewModel(application, userid);
        }
    }
}
