package com.gumbira.application.gumbiratour.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.gumbira.application.gumbiratour.service.model.gmaps.Example;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.Body;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rizky on 25/10/17.
 */

public class MapsRepository {
    private MapsAPI mapsAPI;
    private static MapsRepository mapsRepository;

    private MapsRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mapsAPI = retrofit.create(MapsAPI.class);
    }

    public synchronized static MapsRepository getInstance() {
        if (mapsRepository == null) {
            if (mapsRepository == null) {
                mapsRepository = new MapsRepository();
            }
        }
        return mapsRepository;
    }

    public LiveData<Example> getNearbyPlace(String keyword, String location, String type) {
        final MutableLiveData<Example> data = new MutableLiveData<>();

        mapsAPI.getNearbyPlaces(keyword, location, type).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                data.setValue(response.body());
                System.out.println(response.body().getStatus());
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<Example> getNextPage(String keyword, String type, String pagetoken) {
        final MutableLiveData<Example> data = new MutableLiveData<>();

        mapsAPI.getNextPage(keyword, type, pagetoken).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                data.setValue(response.body());
                System.out.println(response.body().getStatus());
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<ResultDetail> getDetailPlace(String placeid) {
        final MutableLiveData<ResultDetail> data = new MutableLiveData<>();

        mapsAPI.getDetailPlace(placeid).enqueue(new Callback<Body>() {
            @Override
            public void onResponse(Call<Body> call, Response<Body> response) {
                if (data!=null) {
                    data.setValue(response.body().getResult());
                    System.out.println(response.body().getStatus());
                }
            }

            @Override
            public void onFailure(Call<Body> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<List<Result>> getPlaces(final String query, final String type) {
        final MutableLiveData<List<Result>> data = new MutableLiveData<>();

        mapsAPI.getPlaces(query, type).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                data.setValue(response.body().getResults());
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}