package com.gumbira.application.gumbiratour.service.model.firebase;

/**
 * Created by ASUS on 11/02/17.
 */

public class CategoryModel {
    private String city;
    private String id_category;
    private String name_category;
    private String photo;
    private String type;
    private String keyword;

    public CategoryModel() {
    }

    public CategoryModel(String city, String id_category, String name_category, String photo, String type, String keyword) {
        this.city = city;
        this.id_category = id_category;
        this.name_category = name_category;
        this.photo = photo;
        this.type = type;
        this.keyword = keyword;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getName_category() {
        return name_category;
    }

    public void setName_category(String name_category) {
        this.name_category = name_category;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
