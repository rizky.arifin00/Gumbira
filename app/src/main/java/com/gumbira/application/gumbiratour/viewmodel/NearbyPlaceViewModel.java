package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.gumbira.application.gumbiratour.service.model.gmaps.Example;
import com.gumbira.application.gumbiratour.service.repository.MapsRepository;

/**
 * Created by rizky on 25/10/17.
 */

public class NearbyPlaceViewModel extends AndroidViewModel {
    private LiveData<Example> projectListObservable;
    private LiveData<Example> nextpagePlace;

    public NearbyPlaceViewModel(Application application) {
        super(application);

        // If any transformation is needed, this can be simply done by Transformations class ...
    }

    /**
     * Expose the LiveData Projects query so the UI can observe it.
     */
    public LiveData<Example> getProjectListObservable() {
        return projectListObservable;
    }
    public LiveData<Example> getNextpagePlace() {
        return nextpagePlace;
    }

    public void setKey(String keyword, String type, String lat, String lng){
        projectListObservable = MapsRepository.getInstance().getNearbyPlace(keyword, lat + "," + lng,type);
    }

    public void getNextPage(String keyword, String type, String pagetoken){
        nextpagePlace = MapsRepository.getInstance().getNextPage(keyword, type, pagetoken);
    }
}
