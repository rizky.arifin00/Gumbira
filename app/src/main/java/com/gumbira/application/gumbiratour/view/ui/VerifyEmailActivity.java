package com.gumbira.application.gumbiratour.view.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityVerifyEmailBinding;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class VerifyEmailActivity extends AppCompatActivity {
    private ActivityVerifyEmailBinding binding;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_email);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        binding.btnChangeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ChangeEmailActivity.class));
            }
        });
        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                UserShareData.getInstance(getApplicationContext()).removeValue();
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        mAuth.signOut();
        UserShareData.getInstance(this).removeValue();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuth.signOut();
        UserShareData.getInstance(this).removeValue();
    }
}
