package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.repository.FirebaseRepository;

/**
 * Created by rizky on 22/11/17.
 */

public class DetailPackageViewModel extends AndroidViewModel {
    private LiveData<TourPackModel> tourPackObservable;

    public DetailPackageViewModel(Application application, final String packageid, final String state) {
        super(application);
        tourPackObservable = FirebaseRepository.getInstance().getPackageById(packageid, state);
    }

    public LiveData<TourPackModel> getTourPackage(){
        return tourPackObservable;
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final String packageid;
        private final String state;

        public Factory(@NonNull Application application, String packageid, String state) {
            this.application = application;
            this.packageid = packageid;
            this.state = state;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new DetailPackageViewModel(application, packageid, state);
        }
    }
}
