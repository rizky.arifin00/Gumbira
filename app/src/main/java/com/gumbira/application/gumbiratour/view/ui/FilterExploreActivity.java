package com.gumbira.application.gumbiratour.view.ui;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.appyvet.rangebar.RangeBar;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityFilterBinding;
import com.jaredrummler.materialspinner.MaterialSpinner;


public class FilterExploreActivity extends AppCompatActivity {

    private ActivityFilterBinding binding;
    RangeBar range;
    String kota[];
    MaterialSpinner spinnerkota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter);

        setSupportActionBar(binding.filterexpHeader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        binding.filterexpHeader.setTitle("Filter Wisata");
        binding.filterexpHeader.setTitleTextColor(Color.parseColor("#FFFFFF"));


        range = (RangeBar) findViewById(R.id.filterexp_range);
        kota = new String[]{"Jakarta", "Bandung", "Yogyakarta", "Bali", "Labuan Bajo", "Raja Ampat"};

        binding.filterexpSpinnerkota.setItems(kota);

        binding.filterexpSpinnernegara.setItems("Indonesia", "Singapore", "Malaysia", "Jepang", "Korea Selatan");
        binding.filterexpSpinnernegara.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position == 0){
                    kota = new String[]{"Jakarta", "Bandung", "Yogyakarta", "Bali", "Labuan Bajo", "Raja Ampat"};
                }
                else if (position == 1){
                    kota = new String[]{"Singapore"};
                }
                else if (item.equals("Malaysia")){
                    kota = new String[]{"Kuala Lumpur", "Johor Bahru", "Kuching", "Shah Alam", "Ipoh"};
                }
                else if (item.equals("Jepang")){
                    kota = new String[]{"Tokyo", "Kyoto", "Osaka", "Yokohama", "Nagoya"};
                }
                else if (item.equals("Korea Selatan")){
                    kota = new String[]{"Seoul", "Jeju", "Busan", "Gyeongju", "Ulsan"};
                }
                binding.filterexpSpinnerkota.setItems(kota);
            }
        });

        binding.filterexpSpinnerkategori.setItems("Wisata Alam", "Bangunan Bersejarah", "Hiburan", "Tempat Belanja", "Tur Kota");
        binding.filterexpSpinnerkategori.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();

            }
        });

        range.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {

            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue, String rightPinValue) {
            }
        });
    }
}
