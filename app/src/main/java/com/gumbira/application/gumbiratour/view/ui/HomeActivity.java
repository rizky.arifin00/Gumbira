package com.gumbira.application.gumbiratour.view.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.MyApplication;
import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.adapter.ListPromoAdapter;
import com.gumbira.application.gumbiratour.view.adapter.ListPopulerAdapter;
import com.gumbira.application.gumbiratour.databinding.ActivityHomeBinding;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.view.callback.PlaceClickCallback;
import com.gumbira.application.gumbiratour.view.helper.ConnectionReceiver;
import com.gumbira.application.gumbiratour.view.helper.MapsHelper;
import com.gumbira.application.gumbiratour.view.sharedata.CityShareData;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.viewmodel.PlacesViewModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener, ConnectionReceiver.ConnectionReceiverListener {
    private CityShareData cityShareData;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener authListener;
    private ActivityHomeBinding binding;
    private String kota;
    private ListPopulerAdapter listPopulerAdapter;
    private ListPromoAdapter promoadapter;
    private MenuItem nav_login, nav_wantogo;
    private RelativeLayout profilenav;
    private TextView tvEmail;
    private CircleImageView imageProfile;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mRef;
    private GoogleApiClient mGoogleApiClient;
    private PlacesViewModel placesViewModel;
    List<TourPackModel> promolist;
    NavigationView navigationView;
    View headerview;
    Menu menu;
    Location location;
    private double mLatitude = 0;
    private double mLongitude = 0;
    private boolean perms;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private UserShareData userShareData;
    private Geocoder gcd;
    private List<Address> addresses;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        gcd = new Geocoder(HomeActivity.this, Locale.getDefault());
        addresses = null;

        mAuth = FirebaseAuth.getInstance();
        userShareData = UserShareData.getInstance(this);

        changeStatusBar();

        mRootRef.child("country").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot datachild : dataSnapshot.getChildren()) {
                    CountryModel countryModel = datachild.getValue(CountryModel.class);

                    if (countryModel.getCountry().equals(cityShareData.getCountry())) {
                        Glide.with(HomeActivity.this).load(countryModel.getPhoto()).centerCrop().into(binding.homeBanner);
                    } else {
                        System.out.println(countryModel.getCountry() + "-" + cityShareData.getCountry());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        promolist = new ArrayList<>();
        promoadapter = new ListPromoAdapter();
        binding.rvPromo.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvPromo.setAdapter(promoadapter);

        binding.setIsLoading(true);

        binding.category1.setOnClickListener(this);
        binding.category2.setOnClickListener(this);
        binding.category3.setOnClickListener(this);
        binding.moreAttr.setOnClickListener(this);
        binding.moreCategory.setOnClickListener(this);
        binding.morePromo.setOnClickListener(this);

        cityShareData = CityShareData.getInstance(HomeActivity.this);

        perms = MapsHelper.checkPermissions(this);

        setupGoogleAPI();

        checkConnection();

        binding.homeCollapseToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        binding.homeCollapseToolbar.setTitle("Home");

        setSupportActionBar(binding.homeToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.homeToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);

//        Glide.with(this).load("http://thewallpaper.co/wp-content/uploads/2016/10/City-at-night-travel-wallpapers-hd-wallpaper-hd-download-free-background-images-amazing-high-definition-4k-2560x1600.jpg")
//                .into(binding.homeBanner);
//        binding.homeBanner.setColorFilter(Color.rgb(117, 117, 117), PorterDuff.Mode.DARKEN);

        listPopulerAdapter = new ListPopulerAdapter(placeClickCallback);
        binding.homeListAttr.setLayoutManager(new LinearLayoutManager(this));
        binding.homeListAttr.setAdapter(listPopulerAdapter);

        placesViewModel = ViewModelProviders.of(this).get(PlacesViewModel.class);

        Glide.with(this).load("https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Universal_CityWalk_Hollywood_3.JPG/1024px-Universal_CityWalk_Hollywood_3.JPG")
                .into(binding.category1);
        Glide.with(this).load("http://campotrincerato.it/wp-content/uploads/2017/07/fashion-on-line.jpg")
                .into(binding.category2);
        Glide.with(this).load("https://s-media-cache-ak0.pinimg.com/originals/09/7d/a6/097da6aa9b6de0f3df46d43e0906c565.jpg")
                .into(binding.category3);
        navigationView = findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        ImageView imageView = headerview.findViewById(R.id.headerimage);
        Glide.with(this).load(R.drawable.banner2).centerCrop().into(imageView);

        menu = navigationView.getMenu();
        nav_login = menu.findItem(R.id.nav_login);
        nav_wantogo = menu.findItem(R.id.nav_wantogo);
        profilenav = headerview.findViewById(R.id.nav_editprofil);
        tvEmail = headerview.findViewById(R.id.tv_profilname);
        imageProfile = headerview.findViewById(R.id.imageprofile);
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    UserShareData.getInstance(HomeActivity.this).removeValue();
                    onAuthChange();
                }
            }
        };
    }

    private void getPromo(String state) {
        mRootRef.child("package").child(state).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (promolist != null) {
                    promoadapter.setPromolist(new ArrayList<TourPackModel>());
                }
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    TourPackModel tourPackModel = data.getValue(TourPackModel.class);
                    if (!tourPackModel.getPromo().equals("0% OFF")) {
                        promolist.add(tourPackModel);
                    }
                }
                promoadapter.setPromolist(promolist);
                if (promolist.size() == 0) {
                    binding.morePromo.setVisibility(View.GONE);
                    binding.rvPromo.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void explorePlace(String category, String keyword, String type, String kota) {
        Intent i = new Intent(this, ExplorePlaceActivity.class);
        i.putExtra("category", category);
        i.putExtra("keyword", keyword);
        i.putExtra("type", type);
        i.putExtra("kota", kota);
        startActivity(i);
    }

    private void changeStatusBar() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setupGoogleAPI() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    private final PlaceClickCallback placeClickCallback = new PlaceClickCallback() {
        @Override
        public void onClick(Result result) {
            Intent i = new Intent(getApplicationContext(), DetailPlacesActivity.class);
            i.putExtra("id", result.getPlaceId());
            startActivity(i);
        }
    };

    private void onAuthChange() {
        if (userShareData.getValue() != null) {
            tvEmail.setVisibility(View.VISIBLE);
            imageProfile.setVisibility(View.VISIBLE);
            nav_login.setTitle("Sign out");
            nav_wantogo.setVisible(true);
            tvEmail.setText(userShareData.getValue().getUsername());
            Glide.with(HomeActivity.this).load(userShareData.getValue().getImage()).centerCrop().error(R.drawable.banner).into(imageProfile);

            profilenav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.drawerLayout.closeDrawer(GravityCompat.START);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(getApplicationContext(), EditProfilActivity.class));
                        }
                    }, 200);
                }
            });
        } else {
            imageProfile.setVisibility(View.GONE);
            tvEmail.setVisibility(View.GONE);
            nav_login.setTitle("Sign in");
            nav_wantogo.setVisible(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MapsHelper.REQUEST_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getMyLocation();
                } else {
                    finish();
                    Toast.makeText(HomeActivity.this, "Please Grant All Permission to Use All Feature", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void observeViewModel(PlacesViewModel placesViewModel) {
        listPopulerAdapter.setListPopuler(new ArrayList<Result>());
        placesViewModel.setKey("tempat+wisata+hits+di+" + kota, "");
        placesViewModel.getProjectListObservable().observe(this, new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> results) {
                binding.setIsLoading(false);
                if (results != null) {
                    listPopulerAdapter.setListPopuler(results);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        LayoutInflater inflater = (LayoutInflater) this.getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        final View sampleActionView = inflater.inflate(R.layout.layout_toolbar, null);
        MenuItem searchMenuItem = menu.findItem(R.id.nav_search);
        final ImageView search = sampleActionView.findViewById(R.id.nav_cari);
        searchMenuItem.setActionView(sampleActionView);
        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                int[] startingLocation = new int[2];
                startingLocation[0] = (int) (motionEvent.getX() + search.getX());
                startingLocation[1] = (int) (motionEvent.getY() + search.getY());
                SearchActivity.startActivity(startingLocation, HomeActivity.this);
                overridePendingTransition(0, 0);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.nav_search:
//                break;
            case R.id.nav_maps:
                startActivity(new Intent(getApplicationContext(), NearbyPlacesActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authListener);
        onAuthChange();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        if (authListener != null) {
            mAuth.removeAuthStateListener(authListener);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        binding.drawerLayout.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_contact) {
            startActivity(new Intent(HomeActivity.this, ContactActivity.class));
        } else if (id == R.id.nav_explore) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(HomeActivity.this, ExploreActivity.class));
                }
            }, 200);
        } else if (id == R.id.nav_package) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(HomeActivity.this, CountryActivity.class));
                }
            }, 200);

        } else if (id == R.id.nav_wantogo) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(HomeActivity.this, WantToGoActivity.class));
                }
            }, 200);
        } else if (id == R.id.nav_login) {
            if (UserShareData.getInstance(HomeActivity.this).getValue() != null) {
                mAuth.signOut();
                System.out.println("ikan");
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    }
                }, 200);
            }
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (perms) {
            getMyLocation();
            kota = cityShareData.getCity();
            binding.homeTvCity.setText(kota);
            getPromo(cityShareData.getCountry());
            observeViewModel(placesViewModel);
        }
    }

    private void getMyLocation() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(HomeActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(36000);
                    locationRequest.setFastestInterval(36000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(mGoogleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(mGoogleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(HomeActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi
                                                .getLastLocation(mGoogleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically

                                        if (cityShareData.getAddress() == "") {
                                            status.startResolutionForResult(HomeActivity.this,
                                                    REQUEST_CHECK_SETTINGS_GPS);
                                        }
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLongitude == 0) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            if (getCity()) {
                if (addresses.size() > 0) {
                    kota = addresses.get(0).getSubAdminArea().replace("Kota ", "");
                    cityChanged();
                }
            }
        } else {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            if (getCity()) {
                String newkota = addresses.get(0).getSubAdminArea().replace("Kota ", "");
                if (addresses.size() > 0 && !cityShareData.getAddress().equals(addresses.get(0).getThoroughfare())) {
                    kota = newkota;
                    cityChanged();
                }
            }
        }
    }

    private void cityChanged() {
        binding.homeTvCity.setText(kota);
        binding.homeTvCountry.setText(addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName());
        getPromo(addresses.get(0).getCountryName());
        cityShareData.saveCountry(addresses.get(0).getCountryName());
        cityShareData.saveAdminArea(addresses.get(0).getAdminArea());
        cityShareData.saveCity(kota);
        cityShareData.saveAddress(addresses.get(0).getThoroughfare());
        binding.setIsLoading(true);
        observeViewModel(placesViewModel);
    }

    private boolean getCity() {
        try {
            addresses = gcd.getFromLocation(mLatitude, mLongitude, 1);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.category1:
                explorePlace("Things To Do", "things+to+do", "", cityShareData.getCity());
                break;
            case R.id.category2:
                explorePlace("Shopping Center", "shopping+center", "", cityShareData.getCity());
                break;
            case R.id.category3:
                explorePlace("Historical Place", "historical+place", "", cityShareData.getCity());
                break;
            case R.id.more_attr:
                startActivity(new Intent(HomeActivity.this, ExploreActivity.class));
                break;
            case R.id.more_category:
                startActivity(new Intent(HomeActivity.this, ExploreActivity.class));
                break;
            case R.id.more_promo:
                Intent intent = new Intent(HomeActivity.this, TourPackageActivity.class);
                intent.putExtra("state", cityShareData.getCountry());
                intent.putExtra("promo", "ada");
                startActivity(intent);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
//        if (!isConnected) {
//            showDialog();
//        } else {
//            if (dialog.isShowing())
//                dialog.dismiss();
//        }
        showSnack(isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectionListener(this);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        if (!isConnected) {
            message = "No internet connection!";
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
//        Snackbar snackbar = Snackbar
//                .make(binding.coordinatLayout, message, Snackbar.LENGTH_LONG);
//        if (message.equals("Sorry! Not connected to internet")) {
//            snackbar.setAction("RETRY", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    checkConnection();
//                }
//            });
//        }
//
//        // Changing action button text color
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.WHITE);
//
//        snackbar.show();
    }

    private void showDialog() {
        dialog.setContentView(R.layout.dialog_internet);
        Button btnTry = dialog.findViewById(R.id.btnTry);

        btnTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
            }
        });

        if (!dialog.isShowing()) dialog.show();
    }


    private void checkConnection() {
        boolean isConnected = ConnectionReceiver.isConnected();
        showSnack(isConnected);
//        if (!isConnected) {
//            showDialog();
//        } else {
//            if (dialog.isShowing())
//                dialog.dismiss();
//        }
    }
}