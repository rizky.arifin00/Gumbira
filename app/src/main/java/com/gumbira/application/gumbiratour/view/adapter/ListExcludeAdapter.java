package com.gumbira.application.gumbiratour.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemIncludeBinding;

import java.util.List;

/**
 * Created by rizky on 24/11/17.
 */

public class ListExcludeAdapter extends RecyclerView.Adapter<ListExcludeAdapter.viewHolder> {
    List<String> excludeList;

    public void setExcludeList(final List<String> excludeList) {
        this.excludeList = excludeList;
        notifyDataSetChanged();
    }

    @Override
    public ListExcludeAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemIncludeBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_include, parent, false);
        return new ListExcludeAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListExcludeAdapter.viewHolder holder, int position) {
        ItemIncludeBinding binding = holder.binding;
        if (excludeList != null) {
            binding.includeItem.setText(excludeList.get(position));
            binding.iconInclude.setImageResource(R.drawable.ic_clear_blue_24dp);
        } else {
            System.out.println("ikan");
        }
    }

    @Override
    public int getItemCount() {
        if (excludeList != null){
            return excludeList.size();
        }
        return 0;
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemIncludeBinding binding;

        public viewHolder(ItemIncludeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
