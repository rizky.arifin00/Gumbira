package com.gumbira.application.gumbiratour.service.model.firebase;

/**
 * Created by rizky on 10/11/17.
 */

public class WantogoModel {
    private String name, image,  placeid, userid, vicinity,wishid;
    private Float rating;

    public WantogoModel(){}

    public WantogoModel(String name, String image, String placeid, String userid, String vicinity, String wishid, Float rating) {
        this.name = name;
        this.image = image;
        this.placeid = placeid;
        this.userid = userid;
        this.vicinity = vicinity;
        this.wishid = wishid;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getWishid() {
        return wishid;
    }

    public void setWishid(String wishid) {
        this.wishid = wishid;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getStrRating(){
        return String.valueOf(rating);
    }
}
