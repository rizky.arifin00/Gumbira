package com.gumbira.application.gumbiratour.service.model.view;

/**
 * Created by ASUS on 10/13/17.
 */

public class SearchModel {
    private String namawisata;
    private String alamatwisata;
    private String kategori;

    public String getNamawisata() {
        return namawisata;
    }

    public void setNamawisata(String namawisata) {
        this.namawisata = namawisata;
    }

    public String getAlamatwisata() {
        return alamatwisata;
    }

    public void setAlamatwisata(String alamatwisata) {
        this.alamatwisata = alamatwisata;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}
