package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.gumbira.application.gumbiratour.service.repository.FirebaseRepository;

/**
 * Created by rizky on 14/11/17.
 */

public class LoginViewModel extends AndroidViewModel {
    private LiveData<UserModel> userModelObservable;

    public LoginViewModel(Application application, final String userid) {
        super(application);
        userModelObservable = FirebaseRepository.getInstance().getUserById(userid);
    }

    public LiveData<UserModel> getUserModelObservable(){return userModelObservable;}

//    public boolean userLogin(String email, final String password, Activity activity) {
//        mAuth.signInWithEmailAndPassword(email, password)
//                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            hideProgressDialog();
//                            final FirebaseUser user = mAuth.getCurrentUser();
//                            userShareData.save(userModelObservable.getValue(), password);
//                            success = true;
//                        } else {
//                            hideProgressDialog();
//                            Toast.makeText(context, "Email has already used !!     ", Toast.LENGTH_SHORT).show();
//                            success = false;
//                        }
//                    }
//                });
//        return success;
//    }
//
//    public boolean checkUser(){
//        if (mAuth.getCurrentUser() != null){
//            return true;
//        }
//        return false;
//    }
//
//    public void userRegister(final String email, final String username, final String password) {
//        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener((Executor) context, new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                progressDialog.dismiss();
//                if (task.isSuccessful()) {
//                    FirebaseUser user = mAuth.getCurrentUser();
//                    String id = user.getUid();
//                    UserModel userModelData = new UserModel(id, username, email, "");
//                    mRootRef.child("user").child(id).setValue(userModelData);
//                    userShareData.save(userModelData, password);
//                    context.startActivity(new Intent(context, HomeActivity.class));
//                    ((Activity) context).finish();
////                    context.startActivity(new Intent(context, LoginActivity.class));
////                    ((Activity) context).finish();
//                } else {
//                    hideProgressDialog();
//                    Toast.makeText(context, "Email has already used !!     ", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final String userid;

        public Factory(@NonNull Application application, String userid) {
            this.application = application;
            this.userid = userid;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new LoginViewModel(application, userid);
        }
    }
}