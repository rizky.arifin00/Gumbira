package com.gumbira.application.gumbiratour.view.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by rizky on 18/10/17.
 */

public class CostumBindingAdapter {
    @BindingAdapter({"image"})
    public static void loadImage(ImageView view, String url) {
        Glide.with(view.getContext()).load(url).centerCrop().into(view);
    }

//    @BindingAdapter({"imageSmall"})
//    public static void loadImageSmall(ImageView view, String url) {
//        Glide.with(view.getContext()).load(url).centerCrop().into(view);
//    }

    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
