package com.gumbira.application.gumbiratour.view.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityEditProfilBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class EditProfilActivity extends AppCompatActivity {
    private ActivityEditProfilBinding binding;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mRef = mRootRef.child("user");
    private String username, email, image, password;
    private ProgressDialog progressDialog;
    private Uri filePath, link;
    private static final int PICK_IMAGE_REQUEST = 234;
    private UserModel userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profil);
        progressDialog = new ProgressDialog(this);

        binding.editprofilToolbar.setTitle("Edit Profile");
        setSupportActionBar(binding.editprofilToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        userData = UserShareData.getInstance(EditProfilActivity.this).getValue();
        binding.editUsername.setText(userData.getUsername());
        binding.editEmail.setText(userData.getEmail());
        Glide.with(this).load(userData.getImage()).error(R.drawable.ic_user_).placeholder(R.drawable.ic_user_).into(binding.changeImage);

        binding.txtClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        setSupportActionBar(binding.editprofilToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        binding.tvEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditProfilActivity.this, EditPasswordActivity.class));
            }

        });
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        binding.changeImage.setVisibility(View.VISIBLE);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            System.out.println(filePath);
            Glide.with(EditProfilActivity.this).load(filePath).into(binding.changeImage);
        }
    }

    private void uploadimg() {
        progressDialog.setMessage("Editing user ...  ");
        progressDialog.show();
        if (filePath != null) {

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageReference = storage.getReference();

            StorageReference riversRef = storageReference.child("images/" + filePath.getLastPathSegment());
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            link = taskSnapshot.getDownloadUrl();
                            image = link.toString();
                            Toast.makeText(EditProfilActivity.this, "Editing Profile Success !!", Toast.LENGTH_SHORT).show();
                            editUser(image);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //hiding the progress dialog
//                                progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
//                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        }
        //if there is not any file
        else {
            progressDialog.dismiss();
            editUser(image);
        }

    }

    private void editUser(String img) {
        View v = this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        if (img == null) img = UserShareData.getInstance(this).getValue().getImage();
        username = binding.editUsername.getText().toString().trim();
        email = binding.editEmail.getText().toString().trim();

        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, "Please enter User Name", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
        }

        UserModel userModel = new UserModel(userData.getUid(), username, binding.editEmail.getText().toString(), img);
        mRef.child(userData.getUid()).setValue(userModel);
        UserShareData.getInstance(this).save(userModel);
        onBackPressed();
        Toast.makeText(this, "Editing Profile Success !!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editprofil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Edit:
                uploadimg();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
}
