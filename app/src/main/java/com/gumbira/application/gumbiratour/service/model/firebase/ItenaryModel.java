package com.gumbira.application.gumbiratour.service.model.firebase;

import java.io.Serializable;

/**
 * Created by rizky on 25/11/17.
 */

public class ItenaryModel implements Serializable{
    private String Title;
    private String Detail;

    public ItenaryModel(){}

    public ItenaryModel(String title, String detail) {
        Title = title;
        Detail = detail;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }
}
