package com.gumbira.application.gumbiratour.view.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityChangeEmailBinding;
import com.gumbira.application.gumbiratour.view.helper.EmailVerifyHelper;
import com.gumbira.application.gumbiratour.view.helper.FormValidator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangeEmailActivity extends AppCompatActivity {

    private ActivityChangeEmailBinding binding;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_email);

        progressDialog = new ProgressDialog(this);

        binding.changeEmailToolbar.setTitle("Change Email");
        setSupportActionBar(binding.changeEmailToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);


        binding.btnSubmitEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeEmail();
            }
        });
    }

    private void showProgress() {
        progressDialog.setMessage("Changing Email ....");
        progressDialog.show();
    }

    private void hideProgress() {
        progressDialog.dismiss();
    }

    private void changeEmail() {
        View v = this.getCurrentFocus();
        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        FormValidator formValidator = new FormValidator();
        final String email = binding.etChangeEmail.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            binding.etChangeEmail.setError("Please enter email!");
            return;
        } else if (!formValidator.isValidEmail(email)) {
            binding.etChangeEmail.setError("Please enter valid email!");
            return;
        }

        showProgress();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.updateEmail(binding.etChangeEmail.getText().toString().trim())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            EmailVerifyHelper emailVerifyHelper = new EmailVerifyHelper();
                            emailVerifyHelper.verifyEmail(ChangeEmailActivity.this, user);
                            Toast.makeText(getApplicationContext(), "Email address is updated.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Failed to update email!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
