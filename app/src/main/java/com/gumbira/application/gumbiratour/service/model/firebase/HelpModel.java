package com.gumbira.application.gumbiratour.service.model.firebase;

/**
 * Created by dery on 13/10/17.
 */

public class HelpModel {
    private String Keluhan;
    private String Detail;

    public String getKeluhan() {
        return Keluhan;
    }

    public void setKeluhan(String keluhan) {
        Keluhan = keluhan;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public HelpModel(String nama, String detail) {
        this.Keluhan = nama;
        this.Detail = detail;
    }
    public HelpModel(){}
}
