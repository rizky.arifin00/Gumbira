package com.gumbira.application.gumbiratour.service.model.firebase;


public class PromoModel {
    private Integer photo;
    private String nama;
    private String harga;

    public Integer getPhoto() {
        return photo;
    }

    public void setPhoto(Integer photo) {
        this.photo = photo;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public PromoModel(Integer photo, String nama, String harga) {
        this.photo = photo;
        this.nama = nama;
        this.harga = harga;
    }

    public PromoModel(){}
}
