package com.gumbira.application.gumbiratour.service.model.firebase;

import java.io.Serializable;

/**
 * Created by dery on 05/12/17.
 */

public class FilterPackage implements Serializable {
    private String country;
    private String category;
    private String long_day;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLong_day() {
        return long_day;
    }

    public void setLong_day(String long_day) {
        this.long_day = long_day;
    }

    public FilterPackage(String country, String category, String long_day) {
        this.country = country;
        this.category = category;
        this.long_day = long_day;
    }
}
