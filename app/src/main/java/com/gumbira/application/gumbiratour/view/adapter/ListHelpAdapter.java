package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemHelpBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.HelpModel;
import com.gumbira.application.gumbiratour.view.ui.DetailHelpActivity;

import java.util.List;

/**
 * Created by dery on 13/10/17.
 */

public class ListHelpAdapter extends RecyclerView.Adapter<ListHelpAdapter.viewHolder> {

    Context c;
    List<? extends HelpModel> listHelp;

    public ListHelpAdapter(Context c) {
        this.c = c;
    }

    @Override
    public ListHelpAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemHelpBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_help, parent, false);
        return new viewHolder(binding);
    }

    public void setListHelp(final List<? extends HelpModel> listHelp) {
        this.listHelp = listHelp;
        System.out.println(listHelp.size());
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ListHelpAdapter.viewHolder holder, final int position) {
//        tourPackage = packageList.get(position);
//        if(packageList.get(position)!=null) {
//            holder.namaPackage.setText(tourPackage.getNamaPackage());
//            holder.hargaPackage.setText("IDR "+tourPackage.getHargaPackage());
//            holder.fotoPackage.setImageResource(tourPackage.getPhotoPackage());
//        }
        ItemHelpBinding binding = holder.binding;
        binding.setHelp(listHelp.get(position));

        binding.helpItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, DetailHelpActivity.class);
                intent.putExtra("nama", listHelp.get(position).getKeluhan());
                c.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount() {
        if (listHelp != null) {
            return listHelp.size();
        } else {
            return 0;
        }
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemHelpBinding binding;

        public viewHolder(ItemHelpBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
