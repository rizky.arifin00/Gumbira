package com.gumbira.application.gumbiratour.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityImageGalleryBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.view.adapter.ImageGalleryAdapter;
import com.gumbira.application.gumbiratour.viewmodel.DetailPlaceViewModel;

public class ImageGalleryActivity extends AppCompatActivity {
    private Bundle b;
    private String id;
    private DetailPlaceViewModel detailPlaceViewModel;
    private ActivityImageGalleryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_gallery);

        binding.galleryToolbar.setTitle("Photos");
        setSupportActionBar(binding.galleryToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        b = getIntent().getExtras();
        if (b != null) {
            id = b.getString("id");
            DetailPlaceViewModel.Factory factory = new DetailPlaceViewModel.Factory(this.getApplication(), id);
            detailPlaceViewModel = ViewModelProviders.of(this, factory).get(DetailPlaceViewModel.class);
            observeViewModel(detailPlaceViewModel);
        }

        binding.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DetailImgGaleryActivity.class);
                i.putExtra("id", b.getString("id"));
                i.putExtra("position", position);
                startActivity(i);
            }
        });
    }

    private void observeViewModel(DetailPlaceViewModel detailPlaceViewModel) {
        detailPlaceViewModel.getPlaceObservable().observe(this, new Observer<ResultDetail>() {
            @Override
            public void onChanged(@Nullable ResultDetail result) {
                if (result != null) {
                    binding.gridView.setAdapter(new ImageGalleryAdapter(getApplicationContext(), result.getPhotos()));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}