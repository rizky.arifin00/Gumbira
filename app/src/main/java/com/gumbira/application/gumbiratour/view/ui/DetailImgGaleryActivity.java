package com.gumbira.application.gumbiratour.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.eftimoff.viewpagertransformers.ZoomOutSlideTransformer;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityDetailImgGaleryBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.Photo;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.view.adapter.ImageSlideAdapter;
import com.gumbira.application.gumbiratour.viewmodel.DetailPlaceViewModel;

import java.util.List;

public class DetailImgGaleryActivity extends AppCompatActivity {
    private ActivityDetailImgGaleryBinding binding;
    private DetailPlaceViewModel detailPlaceViewModel;
    private List<Photo> foto;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_img_galery);

        binding.detailImgToolbar.setTitle("");
        setSupportActionBar(binding.detailImgToolbar);

        binding.detailImgPager.setClipToPadding(false);
        binding.detailImgPager.setPageMargin(20);
        binding.detailImgPager.setPageTransformer(true, new ZoomOutSlideTransformer());
        Intent i = getIntent();
        String id = i.getExtras().getString("id");
        position = i.getExtras().getInt("position");
        DetailPlaceViewModel.Factory factory = new DetailPlaceViewModel.Factory(this.getApplication(), id);
        detailPlaceViewModel = ViewModelProviders.of(this, factory).get(DetailPlaceViewModel.class);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        observeViewModel(detailPlaceViewModel);
    }

    private void observeViewModel(DetailPlaceViewModel detailPlaceViewModel) {
        detailPlaceViewModel.getPlaceObservable().observe(this, new Observer<ResultDetail>() {
            @Override
            public void onChanged(@Nullable ResultDetail result) {
                if (result != null) {
                    for (int i = 0; i < 9; i++) {
                        foto = result.getPhotos();
                    }
                    initImageSlide();
                }
            }
        });
    }

    private void initImageSlide() {
        binding.detailImgPager.setAdapter(new ImageSlideAdapter(DetailImgGaleryActivity.this, foto, false));
        binding.detailImgPager.setCurrentItem(position);
        binding.detailImgIndicator.setViewPager(binding.detailImgPager);
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_image_gallery, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.clear_back:
//                Toast.makeText(this, "back", Toast.LENGTH_SHORT).show();
//                onBackPressed();
//                break;
//        }
//        return false;
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
