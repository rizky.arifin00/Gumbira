package com.gumbira.application.gumbiratour.view.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityCountryBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;
import com.gumbira.application.gumbiratour.view.adapter.ListCountryAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CountryActivity extends AppCompatActivity {

    private ActivityCountryBinding binding;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private ListCountryAdapter adapter;
    private List<CountryModel> listCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_country);
        adapter = new ListCountryAdapter();

        binding.countryToolbar.setTitle("Tour Package");
        setSupportActionBar(binding.countryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        binding.listCountry.setAdapter(adapter);

        listCountry = new ArrayList<>();

        mRootRef.child("country").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    CountryModel countryModel = childData.getValue(CountryModel.class);
                    listCountry.add(countryModel);
                }
                adapter.setListCountry(listCountry);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}
