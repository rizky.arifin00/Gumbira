package com.gumbira.application.gumbiratour.service.model.firebase;

/**
 * Created by dery on 26/10/17.
 */

public class UserModel {
    private String uid;
    private String username;
    private String email;
    private String image;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public UserModel(String uid, String username, String email, String image) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.image = image;
    }

    public UserModel(){

    }
}
