package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemWisataTerdekatBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.callback.PlaceClickCallback;
import com.gumbira.application.gumbiratour.view.ui.DetailPlacesActivity;

import java.util.List;


/**
 * Created by rizky on 10/10/17.
 */

public class WisataTerdekatAdapter extends RecyclerView.Adapter<WisataTerdekatAdapter.viewHolder> {
    List<Result> listWisata;
    private Context c;

    @Nullable
    private final PlaceClickCallback placeClickCallback;

    public WisataTerdekatAdapter(@Nullable PlaceClickCallback placeClickCallback) {
        this.placeClickCallback = placeClickCallback;
    }

    public void setListWisata(final List<Result> listWisata) {
        this.listWisata = listWisata;
        System.out.println(listWisata.size());
        notifyDataSetChanged();
    }

    public void addListWisata(final Result listWisata) {
        this.listWisata.add(listWisata);
        notifyDataSetChanged();
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        c = parent.getContext();
        ItemWisataTerdekatBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_wisata_terdekat, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {
        ItemWisataTerdekatBinding binding = holder.binding;
        binding.setPlace(listWisata.get(position));
        binding.itemWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPlacesActivity.class);
                i.putExtra("id", listWisata.get(position).getPlaceId());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listWisata != null) {
            return listWisata.size();
        } else {
            return 0;
        }
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        private ItemWisataTerdekatBinding binding;

        public viewHolder(ItemWisataTerdekatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
