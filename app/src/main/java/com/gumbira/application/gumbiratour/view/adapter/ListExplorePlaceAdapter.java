package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemExploreBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.ui.DetailPlacesActivity;

import java.util.List;

/**
 * Created by dery on 01/11/17.
 */

public class ListExplorePlaceAdapter extends RecyclerView.Adapter<ListExplorePlaceAdapter.viewHolder> {
    List<? extends Result> listPlace;
    private Context c;

    public void setListPlace(final List<? extends Result> listPlace) {
        this.listPlace = listPlace;
        System.out.println(listPlace.size());
        notifyDataSetChanged();
    }


    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemExploreBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_explore, parent, false);
        c = parent.getContext();
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, final int position) {
        ItemExploreBinding binding = holder.binding;
        binding.setExplore(listPlace.get(position));
        binding.exploreCvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPlacesActivity.class);
                i.putExtra("id", listPlace.get(position).getPlaceId());
                c.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(listPlace != null){
            return listPlace.size();
        }else{
            return 0;
        }
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        private ItemExploreBinding binding;

        public viewHolder(ItemExploreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}