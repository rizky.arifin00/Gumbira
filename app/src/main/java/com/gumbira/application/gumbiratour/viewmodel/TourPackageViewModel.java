package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.repository.FirebaseRepository;

import java.util.List;

/**
 * Created by rizky on 18/11/17.
 */

public class TourPackageViewModel extends AndroidViewModel {
    private LiveData<List<TourPackModel>> tourPackObservable;

    public TourPackageViewModel(Application application, final String state, final String promo) {
        super(application);
        tourPackObservable = FirebaseRepository.getInstance().getTourPackage(state, promo);
    }

    public LiveData<List<TourPackModel>> getListPackage(){
        return tourPackObservable;
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;
        private final String state;
        private final String promo;

        public Factory(@NonNull Application application, String state, String promo) {
            this.application = application;
            this.state = state;
            this.promo = promo;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new TourPackageViewModel(application, state, promo);
        }
    }
}
