package com.gumbira.application.gumbiratour.view.adapter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;

import java.util.List;

/**
 * Created by Pankaj on 10/27/2017.
 */

public class ListCityAdapter extends BaseExpandableListAdapter {

    private List<? extends CountryModel> countryList;

    public void setCountryList(final List<? extends CountryModel> countryList) {
        this.countryList = countryList;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        if (countryList != null) {
            return countryList.size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (countryList != null){
            return countryList.get(groupPosition).getCity().size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (countryList != null){
            return countryList.get(groupPosition).getCountry();
        }
        return 0;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (countryList != null){
            return countryList.get(groupPosition).getCity().get(childPosition);
        }
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        TextView txtView = new TextView(parent.getContext());
        txtView.setText(countryList.get(groupPosition).getCountry());
        txtView.setPadding(70, 8, 0, 8);
        txtView.setTextColor(Color.BLACK);
        txtView.setTextSize(16);
        return txtView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent) {
        TextView txtView = new TextView(parent.getContext());
        txtView.setText(countryList.get(groupPosition).getCity().get(childPosition));
        txtView.setPadding(75, 8, 8, 8);
        txtView.setTextSize(14);
//        final String ikan = txtView.getText().toString();
//        txtView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("kota", txtView.getText().toString());
//                editor.putString("kota", ikan);
//                editor.commit();
//            }
//        });
        return txtView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
