package com.gumbira.application.gumbiratour.view.costumview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;


import com.gumbira.application.gumbiratour.R;

/**
 * Created by rizky on 13/12/17.
 */

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {
    private int mColor;
    private Paint paint;

    public CustomTextView (Context context) {
        super(context);
        init(context);
    }

    public CustomTextView (Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomTextView (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        Resources resources = context.getResources();
        //Color
        mColor = resources.getColor(R.color.Black);

        paint = new Paint();
        paint.setColor(mColor);
        //Width
        paint.setStrokeWidth(3);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0, (getHeight()/2)+2, getWidth(), (getHeight()/2)+2, paint);
    }
}
