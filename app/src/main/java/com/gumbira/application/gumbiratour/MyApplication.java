package com.gumbira.application.gumbiratour;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.gumbira.application.gumbiratour.view.helper.ConnectionReceiver;

/**
 * Created by rizky on 19/12/17.
 */

public class MyApplication extends Application {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectionListener(ConnectionReceiver.ConnectionReceiverListener listener) {
        ConnectionReceiver.connectionReceiverListener = listener;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
