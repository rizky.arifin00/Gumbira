package com.gumbira.application.gumbiratour.service.model.firebase;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by ASUS on 11/06/17.
 */

public class TourPackModel implements Serializable{
    private List<String> photos;
    private String name_package;
    private String origin_price;
    private String photo;
    private String id_package;
    private String detail;
    private String promo_price;
    private String long_day;
    private String availability;
    private String origin;
    private String min_age;
    private String max_people;
    private String destination;
    private List<String> include;
    private List<String> exclude;
    private List<ItenaryModel> itenary;
    private String latitude;
    private String longitude;
    private String placeid;
    private String state;
    private String location;
    private String promo;
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPromo() {
        return promo+"% OFF";
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    private String Category;

    public TourPackModel(String category) {
        Category = category;
    }


    public TourPackModel() {
    }

    public List<String> getPhotos() {
        System.out.println(photos.size());
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getName_package() {
        return name_package;
    }

    public void setName_package(String name_package) {
        this.name_package = name_package;
    }

    public String getOrigin_price() {
        return formatCurrency(origin_price);
    }

    public void setOrigin_price(String origin_price) {
        this.origin_price = origin_price;
    }

    public String getId_package() {
        return id_package;
    }

    public void setId_package(String id_package) {
        this.id_package = id_package;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPromo_price() {
        return formatCurrency(promo_price);
    }

    public void setPromo_price(String promo_price) {
        this.promo_price = promo_price;
    }

    public String getLong_day() {
        return long_day;
    }

    public void setLong_day(String long_day) {
        this.long_day = long_day;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMin_age() {
        return min_age;
    }

    public void setMin_age(String min_age) {
        this.min_age = min_age;
    }

    public String getMax_people() {
        return max_people;
    }

    public void setMax_people(String max_people) {
        this.max_people = max_people;
    }

    public String getPhoto() {
        if (photos != null) {
            return photos.get(0);
        }
        return "";
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<String> getInclude() {
        System.out.println(include.size());
        return include;
    }

    public void setInclude(List<String> include) {
        this.include = include;
    }

    public List<String> getExclude() {
        return exclude;
    }

    public void setExclude(List<String> exclude) {
        this.exclude = exclude;
    }

    public List<ItenaryModel> getItenary() {
        return itenary;
    }

    public void setItenary(List<ItenaryModel> itenary) {
        this.itenary = itenary;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    private String formatCurrency(String price){
        Locale locale = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(locale);
        return formatRupiah.format(Double.parseDouble(price));
    }
}
