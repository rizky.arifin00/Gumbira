package com.gumbira.application.gumbiratour.view.ui;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityDetailPlaceBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.model.firebase.WantogoModel;
import com.gumbira.application.gumbiratour.service.model.gmaps.Photo;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.view.adapter.ImageSlideAdapter;
import com.gumbira.application.gumbiratour.view.adapter.ListReviewAdapter;
import com.gumbira.application.gumbiratour.view.helper.MapsHelper;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.viewmodel.DetailPlaceViewModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.io.Serializable;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class DetailPlacesActivity extends AppCompatActivity implements LocationListener {
    private ActivityDetailPlaceBinding binding;
    private Bundle b;
    private List<Photo> photos;
    private DetailPlaceViewModel detailPlaceViewModel;
    private GoogleMap mGoogleMap;
    private double mLatitude = 0;
    private double mLongitude = 0;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private ListReviewAdapter adapter;
    private ResultDetail place;
    private String vicinity, name, placeid, userid, image, wishid;
    private Float rating;
    private UserShareData userShareData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_place);

        adapter = new ListReviewAdapter();
        binding.detailWisataReviewList.setLayoutManager(new LinearLayoutManager(this));
        binding.detailWisataReviewList.setAdapter(adapter);
        b = getIntent().getExtras();

        userShareData = UserShareData.getInstance(this);
        mRootRef.child("package").child("Indonesia").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    final TourPackModel tourPackModel = data.getValue(TourPackModel.class);
                    if (tourPackModel.getPlaceid().equals(b.getString("id"))) {
                        binding.packageWrap.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), DetailPackageActivity.class);
                                i.putExtra("id", tourPackModel.getId_package());
                                i.putExtra("state", tourPackModel.getState());
                                startActivity(i);
                            }
                        });
                        binding.packageWrap.setVisibility(View.VISIBLE);
                        binding.detailWisataPackage.setVisibility(View.VISIBLE);
                        binding.detailWisataPackageName.setText(tourPackModel.getName_package());
                        binding.tvHargaOriginal.setText(tourPackModel.getOrigin_price());
                        binding.tvHargaPromo.setText(tourPackModel.getPromo_price());
                        Glide.with(getApplicationContext()).load(tourPackModel.getPhoto()).into(binding.promoLabel);
                        if (!tourPackModel.getPromo().equals("0% OFF")) {
                            binding.promoLabel.setLabelText(tourPackModel.getPromo());
                            binding.tvHargaOriginal.setPaintFlags(binding.tvHargaOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        } else {
                            binding.tvHargaPromo.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        binding.iconWantogo.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (name != null) {
                    if (userShareData.getValue() != null) {
                        likeButton.setLiked(true);
                        addWishList();
                    } else {
                        likeButton.setLiked(false);
                        startActivity(new Intent(DetailPlacesActivity.this, LoginActivity.class));
                    }
                }
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                deleteWishList();
            }
        });

        binding.detailCollapseToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        setSupportActionBar(binding.detailToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        boolean perms = MapsHelper.checkPermissions(this);
        if (perms) {
            mapReady();
        }
        if (b != null) {
            System.out.println(b.getString("id"));
            DetailPlaceViewModel.Factory factory = new DetailPlaceViewModel.Factory(this.getApplication(), b.getString("id"));
            detailPlaceViewModel = ViewModelProviders.of(this, factory).get(DetailPlaceViewModel.class);
            observeViewModel(detailPlaceViewModel);
        }
    }

    private void mapReady() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.nearby_map);

            fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    initMap();
                }
            });
        }
    }

    private void initMap() {
        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mGoogleMap.setMyLocationEnabled(false);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        }
    }

    private void observeViewModel(DetailPlaceViewModel detailPlaceViewModel) {
        detailPlaceViewModel.getPlaceObservable().observe(this, new Observer<ResultDetail>() {
            @Override
            public void onChanged(@Nullable ResultDetail result) {
                if (result != null) {
                    place = result;
                    binding.setPlace(result);
                    binding.detailCollapseToolbar.setTitle(result.getName());
                    ;
                    name = result.getName();
                    vicinity = result.getFormattedAddress();
                    placeid = result.getPlaceId();
                    rating = result.getRatingFloat();
                    image = result.getPhotos().get(0).getPhotoReference();

                    if (result.getReviews() == null) {
                        binding.detailWisataReview.setVisibility(View.GONE);
                        binding.detailWisataReviewList.setVisibility(View.GONE);
                    }

                    if (name != null) {
                        if (userShareData.getValue() != null) {
                            checkWishList();
                        }
                    }

                    if (result.getOpeningHours() != null) {
                        String color = result.getOpeningHours().getOpenNow() == true ? "#9900E676" : "#99FF5252";
                        System.out.println(color);
                        binding.detailWisataOpennow.setTextColor(Color.parseColor(color));
                    }
                    setPlaceLocation(result);
                    adapter.setList(result.getReviews());
                    photos = result.getPhotos();
                    initImageSlide();
                    binding.tvPhoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getApplicationContext(), ImageGalleryActivity.class);
                            i.putExtra("id", place.getPlaceId());
                            startActivity(i);
                        }
                    });
                    mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            Intent i = new Intent(DetailPlacesActivity.this, NearbyPlacesActivity.class);
                            i.putExtra("place", (Serializable) place);
                            startActivity(i);
                        }
                    });

                }
            }
        });
    }

    private void setPlaceLocation(ResultDetail result) {
        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_nature, null);
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(result.getGeometry().getLocation().getLat(),
                result.getGeometry().getLocation().getLng());
        markerOptions.position(latLng);
        markerOptions.title(result.getName());
        markerOptions.snippet("Rating : " + result.getRating());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(MapsHelper.createDrawableFromView(this, marker)));

        Marker m = mGoogleMap.addMarker(markerOptions);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void initImageSlide() {
        binding.detailPager.setAdapter(new ImageSlideAdapter(this, photos, true));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.detail_indicator);
        indicator.setViewPager(binding.detailPager);
    }

    private void addWishList() {
        userid = UserShareData.getInstance(this).getValue().getUid();
        String id = mRootRef.child("wishlist").push().getKey();
        WantogoModel wantogoModel = new WantogoModel(name, image, placeid, userid, vicinity, id, rating);
        mRootRef.child("wishlist").child(userid).child(id).setValue(wantogoModel);
    }

    private void deleteWishList() {
        userid = UserShareData.getInstance(this).getValue().getUid();
        mRootRef.child("wishlist").child(userid).child(wishid).removeValue();
    }

    private void checkWishList() {
        userid = UserShareData.getInstance(this).getValue().getUid();
        mRootRef.child("wishlist").child(userid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    WantogoModel wantogoModel = data.getValue(WantogoModel.class);
                    System.out.println(wantogoModel.getPlaceid() + "-" + placeid);
                    if (wantogoModel.getPlaceid().equals(placeid)) {
                        binding.iconWantogo.setLiked(true);
                        wishid = wantogoModel.getWishid();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}
