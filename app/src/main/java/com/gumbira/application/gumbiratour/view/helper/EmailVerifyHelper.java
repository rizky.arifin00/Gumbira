package com.gumbira.application.gumbiratour.view.helper;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by rizky on 21/11/17.
 */

public class EmailVerifyHelper {
//    private static EmailVerifyHelper emailVerifyHelper;
//    private FirebaseAuth mAuth;
//    private FirebaseUser user;
//
//    private EmailVerifyHelper(){
//        mAuth = FirebaseAuth.getInstance();
//        user = mAuth.getCurrentUser();
//    }
//
//    public synchronized static EmailVerifyHelper getInstance(){
//        if(emailVerifyHelper == null){
//            if(emailVerifyHelper == null){
//                emailVerifyHelper = new EmailVerifyHelper();
//            }
//        }
//        return emailVerifyHelper;
//    }
//
//    public boolean checkVerify(){
//        if(!user.isEmailVerified()){
//            return false;
//        }
//        return true;
//    }
//
//    public FirebaseUser getUser(){
//        return user;
//    }
//
//    public FirebaseAuth getmAuth(){
//        return mAuth;
//    }

    private boolean success;

    public boolean verifyEmail(final Activity activity, FirebaseUser user){
        user.sendEmailVerification().addOnCompleteListener(activity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isComplete()){
                    activity.onBackPressed();
                    Toast.makeText(activity, "Email verifation has sent to your email", Toast.LENGTH_SHORT).show();
                    success = true;
                }
                else {
                    System.out.println(task.getException().getMessage());
                    Toast.makeText(activity, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    success = false;
                }
            }
        });
        return success;
    }
}
