package com.gumbira.application.gumbiratour.view.ui;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.gumbira.application.gumbiratour.R;
import com.appyvet.rangebar.RangeBar;
import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;
import com.gumbira.application.gumbiratour.service.model.firebase.FilterPackage;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.gumbira.application.gumbiratour.databinding.ActivityFilterPackageBinding;

import java.util.ArrayList;
import java.util.List;

public class FilterPackageActivity extends AppCompatActivity {
    RangeBar range;
    private String country;
    private ActivityFilterPackageBinding binding;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private List<String> listCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter_package);
        country = "Indonesia";
        listCountry = new ArrayList<>();

        setSupportActionBar(binding.filtertourToolbar);

        binding.filtertourToolbar.setTitle("Filter Data");
        binding.filtertourToolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRootRef.child("country").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot datachild : dataSnapshot.getChildren()) {
                    CountryModel countryModel = datachild.getValue(CountryModel.class);
                    listCountry.add(countryModel.getCountry());
                }

                binding.filtertourSpinnerKota.setItems(listCountry);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        binding.filtertourSpinnerKota.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                country = item;
            }
        });

        binding.btnFilterTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String daytour = null;
                String jenistour = null;
                int iddaytour = binding.rgDayTour.getCheckedRadioButtonId();
                RadioButton rbDayTour = findViewById(iddaytour);
                if (rbDayTour != null) {
                    daytour = rbDayTour.getText().toString();
                    if (iddaytour == R.id.rbMoreThreeDay) {
                        daytour = "4";
                    }
                }
                int idjenistour = binding.rgJenisTour.getCheckedRadioButtonId();
                RadioButton rbJenisTour = findViewById(idjenistour);
                if (rbJenisTour != null) {
                    jenistour = rbJenisTour.getText().toString();
                }
                FilterPackage filterPackage = new FilterPackage(country, jenistour, daytour);
                Intent intent = new Intent();
                intent.putExtra("filter", filterPackage);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });


//        binding.range.setFormatter(new IRange BarFormatter() {
//            @Override
//            public String format(String s) {
//                int d = binding.;
//                return String.valueOf(d);
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}
