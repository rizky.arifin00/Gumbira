package com.gumbira.application.gumbiratour.view.ui;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityMapsBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.model.gmaps.Example;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.adapter.WisataTerdekatAdapter;
import com.gumbira.application.gumbiratour.view.callback.PlaceClickCallback;
import com.gumbira.application.gumbiratour.view.helper.MapsHelper;
import com.gumbira.application.gumbiratour.view.sharedata.CityShareData;
import com.gumbira.application.gumbiratour.viewmodel.NearbyPlaceViewModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

import java.util.ArrayList;
import java.util.List;

public class NearbyPlacesActivity extends AppCompatActivity implements LocationListener, View.OnClickListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mGoogleMap;
    Location location;
    private double mLatitude = 0;
    private double mLongitude = 0;
    private boolean perms;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    SlideUp slideList;
    private LatLng latLng;
    ActivityMapsBinding binding;
    private WisataTerdekatAdapter wisataTerdekatAdapter;
    private NearbyPlaceViewModel nearbyPlaceViewModel;
    private GoogleApiClient mGoogleApiClient;
    private Bundle b;
    private ResultDetail result;
    private TourPackModel tourPackModel;
    private ArrayList<Result> resultArrayList;
    private CityShareData cityShareData;
    private String category;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        b = getIntent().getExtras();
        token = null;

        cityShareData = CityShareData.getInstance(this);
        category = null;

        if (b != null) {
            binding.detailPlaceWrap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            if (b.getString("category") != null) {
                category = b.getString("category");
                binding.nearbyToolbar.setTitle(category);
            } else if (b.getSerializable("tourpack") != null) {
                tourPackModel = (TourPackModel) b.getSerializable("tourpack");
                setTourpack(tourPackModel);
            } else {
                setDetailPlace();
            }
        } else {
            binding.nearbyToolbar.setTitle("Wisata Terdekat");
            binding.address.setText(cityShareData.getAddress());
        }
        binding.setIsLoading(true);

        setSupportActionBar(binding.nearbyToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        nearbyPlaceViewModel = ViewModelProviders.of(this).get(NearbyPlaceViewModel.class);

        wisataTerdekatAdapter = new WisataTerdekatAdapter(placeClickCallback);
        binding.nearbyListPlaces.setLayoutManager(new LinearLayoutManager(this));
        binding.nearbyListPlaces.setAdapter(wisataTerdekatAdapter);

        perms = MapsHelper.checkPermissions(this);
        setupGoogleAPI();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nearby_map);
        mapFragment.getMapAsync(this);
        listUp();
//        binding.btnLoadMore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (token != null) {
//                    getNextPage();
//                }
//            }
//        });

    }

    private void setDetailPlace() {
        result = (ResultDetail) b.getSerializable("place");
        binding.nearbyToolbar.setTitle(result.getName());
        binding.btnUpSlide.setVisibility(View.GONE);
        binding.nearbyCardWrap.setVisibility(View.GONE);
        binding.detailPlaceWrap.setVisibility(View.VISIBLE);

        binding.placeName.setText(result.getName());
        binding.placeAddress.setText(result.getFormattedAddress());
        Glide.with(this).load(result.getPhotos().get(0).getPhotoReference()).centerCrop().into(binding.placeImg);
        binding.placeRate.setText(result.getStrRating());
        binding.placeType.setText(result.getType());
    }

    private void setTourpack(TourPackModel tourpack) {
        binding.nearbyToolbar.setTitle(tourpack.getName_package());
        binding.btnUpSlide.setVisibility(View.GONE);
        binding.nearbyCardWrap.setVisibility(View.GONE);
        binding.detailPlaceWrap.setVisibility(View.VISIBLE);

        binding.placeName.setText(tourpack.getName_package());
        binding.placeAddress.setText(tourpack.getLocation());
        Glide.with(this).load(tourpack.getPhoto()).centerCrop().into(binding.placeImg);
        binding.placeRate.setText("4.5");
        binding.placeType.setText(tourpack.getCategory());
    }

    private void setupGoogleAPI() {
        // initialize Google API Client
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    private void getMyLocation() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(NearbyPlacesActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(mGoogleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(mGoogleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(NearbyPlacesActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi
                                                .getLastLocation(mGoogleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(NearbyPlacesActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    private final PlaceClickCallback placeClickCallback = new PlaceClickCallback() {
        @Override
        public void onClick(Result result) {
            Intent i = new Intent(getApplicationContext(), DetailPlacesActivity.class);
            i.putExtra("id", result.getPlaceId());
            startActivity(i);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MapsHelper.REQUEST_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getMyLocation();
                } else {
                    finish();
                    Toast.makeText(NearbyPlacesActivity.this, "Please Grant All Permission to Use All Feature", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void listUp() {
        slideList = new SlideUpBuilder(binding.slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
//                        binding.btnUpSlide.setAlpha(1-(percent/100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        if (visibility == View.GONE) {
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        binding.btnUpSlide.setOnClickListener(this);
        binding.nearbyCardWrap.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
//        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void observeViewModel(NearbyPlaceViewModel nearbyPlaceViewModel) {
        nearbyPlaceViewModel.setKey("tempat+wisata", "", String.valueOf(mLatitude), String.valueOf(mLongitude));
        nearbyPlaceViewModel.getProjectListObservable().observe(this, new Observer<Example>() {
            @Override
            public void onChanged(@Nullable Example results) {
                binding.setIsLoading(false);
                if (results != null) {
//                    if (results.getNextPageToken() != null) {
//                        Toast.makeText(NearbyPlacesActivity.this, results.getNextPageToken()+"", Toast.LENGTH_SHORT).show();
//                        token = results.getNextPageToken();
//                    }
                    setMarker(results.getResults());
                    binding.tvNear.setText("Neared Location (" + results.getResults().size() + ")");
                }
            }
        });
    }

//    private void getNextPage(){
//        nearbyPlaceViewModel.getNextPage("tempat+wisata","",token);
//        nearbyPlaceViewModel.getNextpagePlace().observe(this, new Observer<Example>() {
//            @Override
//            public void onChanged(@Nullable Example example) {
//                for (int i = 0; i < example.getResults().size(); i++){
//                    wisataTerdekatAdapter.addListWisata(example.getResults().get(i));
//                }
//            }
//        });
//    }

    private void setMarker(final List<Result> results) {
        System.out.println(results.size() + "main");
        wisataTerdekatAdapter.setListWisata(results);
        for (int i = 0; i < results.size(); i++) {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_nature, null);
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(results.get(i).getGeometry().getLocation().getLat(),
                    results.get(i).getGeometry().getLocation().getLng());
            markerOptions.position(latLng);
            markerOptions.title(results.get(i).getName());
            markerOptions.snippet("Rating : " + results.get(i).getRating());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(MapsHelper.createDrawableFromView(NearbyPlacesActivity.this, marker)));

            Marker m = mGoogleMap.addMarker(markerOptions);
            m.setTag(results.get(i));

            mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Result r = (Result) marker.getTag();
                    Intent i = new Intent(NearbyPlacesActivity.this, DetailPlacesActivity.class);
                    i.putExtra("id", r.getPlaceId());
                    startActivity(i);
                }
            });
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLatitude == 0) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            if (b == null) {
                latLng = new LatLng(mLatitude, mLongitude);
                observeViewModel(nearbyPlaceViewModel);

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
            } else {
                binding.setIsLoading(false);
                if (category != null) {
                    resultArrayList = (ArrayList<Result>) b.getSerializable("places");
                    latLng = new LatLng(resultArrayList.get(4).getGeometry().getLocation().getLat(), resultArrayList.get(4).getGeometry().getLocation().getLng());
                    setMarker(resultArrayList);
                    binding.tvNear.setText("Founded Location (" + resultArrayList.size() + ")");
                    binding.address.setText("Category " + category);
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                } else if (tourPackModel != null) {
                    setSingleMarker(Double.parseDouble(tourPackModel.getLatitude()), Double.parseDouble(tourPackModel.getLongitude())
                            , tourPackModel.getName_package(), "4.5");
                } else {
                    setSingleMarker(result.getGeometry().getLocation().getLat(), result.getGeometry().getLocation().getLng(),
                            result.getName(), result.getStrRating());
                }
            }
        }
    }

    private void setSingleMarker(Double lat, Double lng, String name, String rate) {
        latLng = new LatLng(lat, lng);
        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_nature, null);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(name);
        markerOptions.snippet("Rating : " + rate);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(MapsHelper.createDrawableFromView(NearbyPlacesActivity.this, marker)));

        Marker m = mGoogleMap.addMarker(markerOptions);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.nearby_card_wrap || v.getId() == R.id.btnUpSlide) {
            slideList.show();
        }
    }

    @Override
    public void onBackPressed() {
        if (slideList.isVisible()) {
            slideList.hide();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                if (b == null) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
//                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
//                mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
            }
        }
        else {
            if (b == null) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (perms) {
            getMyLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }
    }
}
