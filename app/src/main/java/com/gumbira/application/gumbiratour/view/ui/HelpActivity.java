package com.gumbira.application.gumbiratour.view.ui;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityHelpBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.HelpModel;
import com.gumbira.application.gumbiratour.view.adapter.ListHelpAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HelpActivity extends AppCompatActivity {
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference mRef = mRootRef.child("help");
    RecyclerView recyclerView;
    List<HelpModel> listHelpModel;
    private ActivityHelpBinding binding;
    HelpModel helpModel;
    private ListHelpAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.helpRvHelp.setLayoutManager(layoutManager);

        listHelpModel = new ArrayList<>();
        adapter = new ListHelpAdapter(this);
        binding.helpRvHelp.setAdapter(adapter);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    helpModel = data.getValue(HelpModel.class);
                    listHelpModel.add(helpModel);
                }
                adapter.setListHelp(listHelpModel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        setSupportActionBar(binding.helpBack);

        binding.helpBack.setTitle("HelpModel");
        binding.helpBack.setTitleTextColor(Color.parseColor("#FFFFFF"));
    }
}
