package com.gumbira.application.gumbiratour.view.callback;

import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;

/**
 * Created by rizky on 30/10/17.
 */

public interface PlaceClickCallback {
    void onClick(Result result);
}
