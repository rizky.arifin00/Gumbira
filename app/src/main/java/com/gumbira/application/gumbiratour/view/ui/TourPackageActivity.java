package com.gumbira.application.gumbiratour.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.service.model.firebase.FilterPackage;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.databinding.ActivityTourPackageBinding;
import com.gumbira.application.gumbiratour.view.adapter.ListTourPackAdapter;
import com.gumbira.application.gumbiratour.viewmodel.TourPackageViewModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TourPackageActivity extends AppCompatActivity {
    private ListTourPackAdapter adapter;
    private ActivityTourPackageBinding binding;
    private TourPackageViewModel tourPackageViewModel;
    private List<TourPackModel> tourPackModelList;
    private String category, country, longday;
    private String state, promo;
    private Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tour_package);

        tourPackModelList = new ArrayList<>();
        b = getIntent().getExtras();
        if (b != null) {
            state = b.getString("state");
            promo = b.getString("promo");
        }
        initToolbar();

        adapter = new ListTourPackAdapter();
        binding.listPackage.setAdapter(adapter);
        TourPackageViewModel.Factory factory = new TourPackageViewModel.Factory(this.getApplication(), state, promo);
        tourPackageViewModel = ViewModelProviders.of(this, factory).get(TourPackageViewModel.class);
        observeViewModel(tourPackageViewModel);
    }

    private void observeViewModel(TourPackageViewModel tourPackageViewModel) {
        tourPackageViewModel.getListPackage().observe(this, new Observer<List<TourPackModel>>() {
            @Override
            public void onChanged(@Nullable List<TourPackModel> tourPackModels) {
                if (tourPackModels != null) {
                    adapter.setTourPackList(tourPackModels);
                }
            }
        });
    }

    private void initToolbar() {
        binding.tourpack2Toolbar.setTitle(state);
        setSupportActionBar(binding.tourpack2Toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tour_package, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
            final FilterPackage filterPackage = (FilterPackage) data.getExtras().getSerializable("filter");
            category = filterPackage.getCategory();
            country = filterPackage.getCountry() == null ? "" : filterPackage.getCountry();
            binding.tourpack2Toolbar.setTitle(country);
            longday = filterPackage.getLong_day();
            tourPackModelList.clear();
            binding.tvNotfound.setVisibility(View.GONE);
            binding.listPackage.setVisibility(View.VISIBLE);
            DatabaseReference mRef = mRootRef.child("package").child(country);

            mRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        TourPackModel tourPackModel = data.getValue(TourPackModel.class);
                        if (tourPackModel.getPlaceid() != null) {
                            System.out.println(longday + "-" + category + "-" + country);
                            int firstdayfil = 0;
                            int firstdaytou = Integer.parseInt(String.valueOf(tourPackModel.getLong_day().charAt(0)));

                            if (tourPackModel.getLong_day().contains("Hours")) {
                                System.out.println("1hari");
                                firstdaytou = 1;
                            }

                            if (longday != null) {
                                firstdayfil = Integer.parseInt(String.valueOf(filterPackage.getLong_day().charAt(0)));
                            }

                            if (longday != null && category != null && country != null) {
                                if (category.equals("Promo")) {
                                    if (firstdayfil == 4) {
                                        if (!tourPackModel.getPromo().equals("0% OFF") && firstdaytou >= firstdayfil) {
                                            tourPackModelList.add(tourPackModel);
                                        }
                                    } else {
                                        if (!tourPackModel.getPromo().equals("0% OFF") && firstdayfil == firstdaytou) {
                                            tourPackModelList.add(tourPackModel);
                                        }
                                    }
                                } else {
                                    if (firstdayfil == 4) {
                                        if (category.equals(tourPackModel.getCategory()) && firstdaytou >= firstdayfil) {
                                            tourPackModelList.add(tourPackModel);
                                        }
                                    } else {
                                        if (category.equals(tourPackModel.getCategory()) && firstdayfil == firstdaytou) {
                                            tourPackModelList.add(tourPackModel);
                                        }
                                    }
                                }
                            } else if (category != null) {
                                if (category.equals("Promo")) {
                                    if (!tourPackModel.getPromo().equals("0% OFF")) {
                                        tourPackModelList.add(tourPackModel);
                                    }
                                } else if (category.equals(tourPackModel.getCategory())) {
                                    tourPackModelList.add(tourPackModel);
                                }
                            } else if (longday != null) {
                                if (firstdayfil == 4) {
                                    if (firstdaytou >= firstdayfil) {
                                        tourPackModelList.add(tourPackModel);
                                    }
                                } else {
                                    if (firstdayfil == firstdaytou) {
                                        tourPackModelList.add(tourPackModel);
                                    }
                                }
                            } else {
//                                observeViewModel(tourPackageViewModel);
                            }
                        } else {
                            Toast.makeText(TourPackageActivity.this, "Kosong", Toast.LENGTH_SHORT).show();
                        }
                    }
                    adapter.setTourPackList(tourPackModelList);
                    if (tourPackModelList.size() == 0) {
                        binding.tvNotfound.setVisibility(View.VISIBLE);
                        binding.listPackage.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }

    private void clearItem() {
        tourPackModelList.clear();
        Toast.makeText(TourPackageActivity.this, "Not found item for that filter!", Toast.LENGTH_SHORT).show();
    }

//    private void filterData(List<TourPackModel> tourPackModelList, TourPackModel tourPackModel){
//        if (tourPackModelList.size() == 0){
//            binding.text.setVisibility(View.GONE);
//        }
//        tourPackModelList.add(tourPackModel);
//        adapter.setTourPackList(tourPackModelList);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.Filter:
                Intent intent = new Intent(getApplicationContext(), FilterPackageActivity.class);
                startActivityForResult(intent, 1);
                return true;
        }
        return true;
    }

}
