package com.gumbira.application.gumbiratour.view.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityDetailPackageBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.ItenaryModel;
import com.gumbira.application.gumbiratour.service.model.firebase.OrderModel;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.view.adapter.ListItenaryAdapter;
import com.gumbira.application.gumbiratour.view.adapter.ListExcludeAdapter;
import com.gumbira.application.gumbiratour.view.adapter.ListIncludeAdapter;
import com.gumbira.application.gumbiratour.view.helper.MapsHelper;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.viewmodel.DetailPackageViewModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class DetailPackageActivity extends AppCompatActivity {

    private ListIncludeAdapter listIncludeAdapter;
    private ListExcludeAdapter listExcludeAdapter;
    private ListItenaryAdapter itenaryListAdapter;
    private DetailPackageViewModel detailPackageViewModel;
    private ActivityDetailPackageBinding binding;
    private ArrayList<ItenaryModel> tourPackModelList;
    private GoogleMap mGoogleMap;
    private Bundle b;
    private Dialog dialog;
    private UserShareData userShareData;
    private Button btnSubmit;
    private boolean check;
    private EditText etPhone;
    private String orderid, phone;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private TourPackModel tourPack;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_package);

        builder = new AlertDialog.Builder(DetailPackageActivity.this);
        b = getIntent().getExtras();
        orderid = null;
        tourPackModelList = new ArrayList<>();
        tourPack = new TourPackModel();
        userShareData = UserShareData.getInstance(this);

        initRecyler();

        binding.detailPackageCollapseToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        binding.detailPackageCollapseToolbar.setTitle("");
        itenaryListAdapter = new ListItenaryAdapter();
        setSupportActionBar(binding.detailPackageToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        boolean perms = MapsHelper.checkPermissions(this);
        if (perms) {
            mapReady();
        }

        if (b != null) {
            System.out.println(b.getString("id"));
            DetailPackageViewModel.Factory factory = new DetailPackageViewModel.Factory(this.getApplication(), b.getString("id"), "Indonesia");
            detailPackageViewModel = ViewModelProviders.of(this, factory).get(DetailPackageViewModel.class);
            observeViewModel(detailPackageViewModel);
        }

        binding.btnSeeItenary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailPackageActivity.this, ItenaryActivity.class);
                i.putExtra("itenary", tourPackModelList);
                startActivity(i);
            }
        });

        setDialog();

        binding.btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userShareData.getValue() != null) {
                    if (check) {
                        builder
                                .setMessage("Request is on process, are you want to cancel this process ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                        mRootRef.child("order").child(userShareData.getValue().getUid()).child(orderid).removeValue();
                                        check = false;
                                        Toast.makeText(DetailPackageActivity.this, "Your process is canceled", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                                .show();
                    } else {
                        dialog.show();
                    }
                } else {
                    startActivity(new Intent(DetailPackageActivity.this, LoginActivity.class));
                }
            }
        });
    }

    private void setDialog() {
        dialog = new Dialog(DetailPackageActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_book_now);
        btnSubmit = dialog.findViewById(R.id.btnSubmit);
        etPhone = dialog.findViewById(R.id.et_phone);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phone = etPhone.getText().toString().trim();
                View vk = DetailPackageActivity.this.getCurrentFocus();
                if (vk != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(vk.getWindowToken(), 0);
                }
                if (TextUtils.isEmpty(phone)){
                    etPhone.setError("Please enter phone");
                    return;
                }
                else if (phone.length() < 11){
                    etPhone.setError("Please enter more than 11 number");
                    return;
                }
                dialog.dismiss();
                if (userShareData.getValue() != null) {
                    if (btnSubmit.isEnabled()) {
                        String id = mRootRef.child("order").push().getKey();
                        OrderModel orderModel = new OrderModel(id, tourPack.getId_package(), tourPack.getName_package(),
                                userShareData.getValue().getUid(), userShareData.getValue().getEmail(),
                                etPhone.getText().toString(), "not read");
                        mRootRef.child("order").child(userShareData.getValue().getUid()).child(id).setValue(orderModel);
                        Toast.makeText(DetailPackageActivity.this, "Thank you, your request will be process", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DetailPackageActivity.this, "Your request is being processed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void checkOrder() {
        mRootRef.child("order").child(userShareData.getValue().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataChild : dataSnapshot.getChildren()) {
                    OrderModel orderModel = dataChild.getValue(OrderModel.class);
                    if (orderModel.getTourid().equals(tourPack.getId_package())) {
                        orderid = orderModel.getOrderid();
                        if (orderModel.getStatus().equals("not read")){
                            check = true;
                        }
                        else {
                            check = false;
                        }
                    } else {
                        check = false;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initRecyler() {
        listIncludeAdapter = new ListIncludeAdapter();
        listExcludeAdapter = new ListExcludeAdapter();
        binding.listInclude.setLayoutManager(new LinearLayoutManager(this));
        binding.listInclude.setAdapter(listIncludeAdapter);
        binding.listExclude.setLayoutManager(new LinearLayoutManager(this));
        binding.listExclude.setAdapter(listExcludeAdapter);
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_expand);
        final ExpandableListView expandableListView = dialog.findViewById(R.id.expandableListView);
        TextView tvTitle = dialog.findViewById(R.id.title_dialog);
        tvTitle.setText("Itenary");
        expandableListView.setAdapter(itenaryListAdapter);
        expandableListView.expandGroup(0);
        dialog.show();
    }

    private void mapReady() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.nearby_map);

            fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    initMap();
                }
            });
        }
    }

    private void initMap() {
        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mGoogleMap.setMyLocationEnabled(false);
            mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        }
    }

    private void setPlaceLocation(TourPackModel tourPackModel) {
        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_nature, null);
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(Double.parseDouble(tourPackModel.getLatitude()),
                Double.parseDouble(tourPackModel.getLongitude()));
        markerOptions.position(latLng);
        markerOptions.title(tourPackModel.getName_package());
        markerOptions.snippet("Rating : " + tourPackModel.getOrigin_price());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(MapsHelper.createDrawableFromView(this, marker)));

        Marker m = mGoogleMap.addMarker(markerOptions);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }

    private void observeViewModel(DetailPackageViewModel detailPackageViewModel) {
        detailPackageViewModel.getTourPackage().observe(this, new Observer<TourPackModel>() {
            @Override
            public void onChanged(@Nullable final TourPackModel tourPackModel) {
                if (tourPackModel != null) {
                    binding.setTourpack(tourPackModel);
                    tourPack = tourPackModel;
                    if (userShareData.getValue() != null) {
                        checkOrder();
                    }
                    tourPackModelList = (ArrayList<ItenaryModel>) tourPackModel.getItenary();
                    System.out.println(tourPackModel.getName_package());
                    if (!tourPackModel.getPromo().equals("0% OFF")) {
                        binding.tvHargaOriginal.setPaintFlags(binding.tvHargaOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    } else {
                        binding.tvHargaPromo.setVisibility(View.GONE);
                        binding.tvHargaOriginal.setTextColor(Color.BLACK);
                        binding.tvPromo.setVisibility(View.GONE);
                    }
                    binding.detailPackageCollapseToolbar.setTitle(tourPackModel.getName_package());
                    listIncludeAdapter.setIncludeList(tourPackModel.getInclude());
                    listExcludeAdapter.setExcludeList(tourPackModel.getExclude());
                    itenaryListAdapter.setTourPackList(tourPackModel.getItenary());
                    setPlaceLocation(tourPackModel);


                    mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            Intent i = new Intent(DetailPackageActivity.this, NearbyPlacesActivity.class);
                            i.putExtra("tourpack", (Serializable) tourPackModel);
                            startActivity(i);
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}
