package com.gumbira.application.gumbiratour.view.ui;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gumbira.application.gumbiratour.databinding.ActivityExploreBinding;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.service.model.firebase.CategoryModel;
import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;
import com.gumbira.application.gumbiratour.view.adapter.ListCityAdapter;
import com.gumbira.application.gumbiratour.view.adapter.ListKategoriAdapter;
import com.gumbira.application.gumbiratour.view.sharedata.CityShareData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ExploreActivity extends AppCompatActivity {
    private ActivityExploreBinding binding;
    private DatabaseReference database;
    private DatabaseReference mRef;
    private ListKategoriAdapter listKategoriAdapter;
    private List<CategoryModel> categoryModelList;
    private List<CountryModel> countryModelList;
    private CategoryModel categoryModel;
    private ListCityAdapter adapter;
    private Dialog dialog;
    private ExpandableListView expandableListView;
    private String kota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_explore);

//        binding.exploreToolbar.setTitle("Explore");
//        binding.exploreToolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
//        binding.exploreCollapseToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
//        binding.exploreCollapseToolbar.setTitle("Explore");
        binding.exploreToolbar.setTitle("");
        setSupportActionBar(binding.exploreToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        categoryModelList = new ArrayList<>();
        countryModelList = new ArrayList<>();

        adapter = new ListCityAdapter();
        database = FirebaseDatabase.getInstance().getReference();
        binding.masonryGrid.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        listKategoriAdapter = new ListKategoriAdapter(this);
        binding.masonryGrid.setAdapter(listKategoriAdapter);
//        SpacesItemModel decoration = new SpacesItemModel(8);
//        binding.masonryGrid.addItemDecoration(decoration);
        kota = CityShareData.getInstance(this).getCity();
        binding.kota.setText(kota);
        setDialog();

        binding.kota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        getData(kota);
    }

    private void getCountry() {
        database.child("country").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    CountryModel countryModel = data.getValue(CountryModel.class);
                    System.out.println(countryModel.getCountry() + "-" + countryModel.getCity().get(0));
                    countryModelList.add(countryModel);
                    adapter.setCountryList(countryModelList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getData(final String kota) {
        database.child("category").child(kota).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (categoryModelList != null) {
                    categoryModelList.clear();
                }
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    categoryModel = data.getValue(CategoryModel.class);
                    categoryModelList.add(categoryModel);
                    listKategoriAdapter.setListKategori(categoryModelList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_expand);
        expandableListView = dialog.findViewById(R.id.expandableListView);
        TextView tvTitle = dialog.findViewById(R.id.title_dialog);
        tvTitle.setText("Choose City");
        expandableListView.setAdapter(adapter);

        getCountry();

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                kota = countryModelList.get(groupPosition).getCity().get(childPosition);
                binding.kota.setText(kota);
                getData(kota);
                dialog.dismiss();
                return false;
            }
        });
    }
//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                sharedPreferences = getSharedPreferences(mypref, Context.MODE_PRIVATE);
//                kota = sharedPreferences.getString("kota", "");
//                System.out.println(kota);
//                binding.kota.setText(kota);
//                mRef.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if (categoryModelList != null) {
//                            categoryModelList.clear();
//                        }
//                        for (DataSnapshot data : dataSnapshot.getChildren()) {
//                            categoryModel = data.getValue(CategoryModel.class);
//                            System.out.println(categoryModel.getKota() + "-" + kota);
//                            if (categoryModel.getKota().equals(kota)) {
//                                categoryModelList.add(categoryModel);
//                            }
//                        }
//                        listKategoriAdapter.setListKategori(categoryModelList);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
//            }
//        });


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_explore, menu);
        LayoutInflater inflater = (LayoutInflater) this.getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        final View sampleActionView = inflater.inflate(R.layout.layout_toolbar, null);
        MenuItem searchMenuItem = menu.findItem(R.id.nav_search);
        final ImageView search = sampleActionView.findViewById(R.id.nav_cari);
        searchMenuItem.setActionView(sampleActionView);
        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
//                    Toast.makeText(HomeActivity.this, "hupla", Toast.LENGTH_SHORT).show();
                int[] startingLocation = new int[2];
                startingLocation[0] = (int) (motionEvent.getX() + search.getX());
                startingLocation[1] = (int) (motionEvent.getY() + search.getY());
                SearchActivity.startActivity(startingLocation, ExploreActivity.this);
                overridePendingTransition(0, 0);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.nav_search:
//                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
//                startActivity(intent);
//                return true;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}
