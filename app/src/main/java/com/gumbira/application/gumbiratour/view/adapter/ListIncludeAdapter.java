package com.gumbira.application.gumbiratour.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemIncludeBinding;

import java.util.List;

/**
 * Created by rizky on 23/11/17.
 */

public class ListIncludeAdapter extends RecyclerView.Adapter<ListIncludeAdapter.viewHolder> {
    List<String> includeList;

    public void setIncludeList(final List<String> includeList) {
        this.includeList = includeList;
        notifyDataSetChanged();
    }

    @Override
    public ListIncludeAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemIncludeBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_include, parent, false);
        return new ListIncludeAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListIncludeAdapter.viewHolder holder, int position) {
        ItemIncludeBinding binding = holder.binding;
        if (includeList != null) {
            binding.includeItem.setText(includeList.get(position));
            System.out.println(includeList.get(position));
        } else {
            System.out.println("ikan");
        }
    }

    @Override
    public int getItemCount() {
        if (includeList != null){
            return includeList.size();
        }
        return 0;
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemIncludeBinding binding;

        public viewHolder(ItemIncludeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
