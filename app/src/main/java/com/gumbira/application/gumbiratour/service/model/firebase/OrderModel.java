package com.gumbira.application.gumbiratour.service.model.firebase;

/**
 * Created by rizky on 26/12/17.
 */

public class OrderModel {
    private String orderid;
    private String tourid;
    private String nametour;
    private String userid;
    private String email;
    private String nohp;
    private String status;

    public OrderModel(){}

    public OrderModel(String orderid, String tourid, String nametour, String userid, String email, String nohp, String status) {
        this.orderid = orderid;
        this.tourid = tourid;
        this.nametour = nametour;
        this.userid = userid;
        this.email = email;
        this.nohp = nohp;
        this.status = status;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getTourid() {
        return tourid;
    }

    public void setTourid(String tourid) {
        this.tourid = tourid;
    }

    public String getNametour() {
        return nametour;
    }

    public void setNametour(String nametour) {
        this.nametour = nametour;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
