package com.gumbira.application.gumbiratour.view.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityEditPasswordBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class EditPasswordActivity extends AppCompatActivity {
    private ActivityEditPasswordBinding binding;
    private String email, password, newpassword, verify;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_password);

        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        setSupportActionBar(binding.editPasswordToolbar);
        binding.editPasswordToolbar.setTitle("Edit Password");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        binding.btnSubmitEditPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPass();
            }
        });
    }

    private void editPass() {
        progressDialog.setMessage("Editing Password ....");
        progressDialog.show();
        View v = this.getCurrentFocus();
        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        newpassword = binding.editPasswordBaru.getText().toString().trim();
        verify = binding.editKonfirPassword.getText().toString().trim();

        if (TextUtils.isEmpty(newpassword)) {
            binding.editPasswordBaru.setError("Please enter password!");
            return;
        } else if (newpassword.length() < 6) {
            binding.editPasswordBaru.setError("Please enter more than 6 character!");
            return;
        }
        if (TextUtils.isEmpty(verify)) {
            binding.editKonfirPassword.setError("Please verify password!");
            return;
        } else if (!newpassword.equals(verify)) {
            binding.editKonfirPassword.setError("Password not matched");
            return;
        }
        final FirebaseUser user = mAuth.getCurrentUser();
        System.out.println(user.getEmail());
        user.updatePassword(newpassword).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.dismiss();
                if (task.isSuccessful()) {
                    System.out.println(user.getEmail());
                    Toast.makeText(EditPasswordActivity.this, "Password has changed, log in with new password", Toast.LENGTH_SHORT).show();
                    mAuth.signOut();
                    finish();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                } else {
                    Toast.makeText(EditPasswordActivity.this, "Gagal", Toast.LENGTH_SHORT).show();
                    System.out.println(task.getException().getMessage());
                    task.getException().printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
}