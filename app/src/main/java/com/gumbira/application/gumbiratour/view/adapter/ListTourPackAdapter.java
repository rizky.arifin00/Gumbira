package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemPackage2Binding;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.view.ui.DetailPackageActivity;

import java.util.List;

/**
 * Created by ASUS on 11/06/17.
 */

public class ListTourPackAdapter extends RecyclerView.Adapter<ListTourPackAdapter.viewHolder> {
    List<? extends TourPackModel> tourPackList;
    private Context c;

    public void setTourPackList(final List<? extends TourPackModel> tourPackList) {
        this.tourPackList = tourPackList;
        notifyDataSetChanged();
    }

    @Override
    public ListTourPackAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPackage2Binding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_package2, parent, false);
        c = parent.getContext();
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListTourPackAdapter.viewHolder holder, final int position) {
        ItemPackage2Binding binding = holder.binding;
        binding.setTourpack(tourPackList.get(position));
        if (!tourPackList.get(position).getPromo().equals("0% OFF")) {
            binding.packageImage.setLabelText(tourPackList.get(position).getPromo());
            binding.tvHargaOriginal.setPaintFlags(binding.tvHargaOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            binding.tvHargaPromo.setVisibility(View.GONE);
            binding.tvHargaOriginal.setTextColor(Color.BLACK);
        }
        binding.itemPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DetailPackageActivity.class);
                i.putExtra("id",tourPackList.get(position).getId_package());
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (tourPackList != null) {
            return tourPackList.size();
        }
        return 0;
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemPackage2Binding binding;

        public viewHolder(ItemPackage2Binding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
