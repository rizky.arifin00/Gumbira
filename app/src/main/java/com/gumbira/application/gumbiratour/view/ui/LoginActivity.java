package com.gumbira.application.gumbiratour.view.ui;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityLoginBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.gumbira.application.gumbiratour.view.helper.FormValidator;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.gumbira.application.gumbiratour.viewmodel.LoginViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;
    private String email, password;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
//    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
//    private DatabaseReference mRef = mRootRef.child("user");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        binding.toolbarSignin.setTitle("");
        setSupportActionBar(binding.toolbarSignin);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        progressDialog = new ProgressDialog(this);

        binding.loginBtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

        binding.tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(signup);
            }
        });

        binding.tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUser();
    }

    private void checkUser() {
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().isEmailVerified()) {
            System.out.println("udh login kan entee !!");
            onBackPressed();
        } else if (mAuth.getCurrentUser() != null && !mAuth.getCurrentUser().isEmailVerified()) {
            startActivity(new Intent(this, VerifyEmailActivity.class));
            System.out.println("verify dulu cukk!");
        }
    }

    private void showProgress() {
        progressDialog.setMessage("Logining user ...  ");
        progressDialog.show();
    }

    private void hideProgress() {
        progressDialog.dismiss();
    }

    private void userLogin() {
        View v = this.getCurrentFocus();
        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        FormValidator formValidator = new FormValidator();
        email = binding.loginEmail.getText().toString().trim();
        password = binding.loginPassword.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            binding.loginEmail.setError("Please enter email!");
            return;
        } else if (!formValidator.isValidEmail(email)) {
            binding.loginEmail.setError("Please enter valid email!");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            binding.loginPassword.setError("Please enter password!");
            return;
        } else if (password.length() < 6) {
            binding.loginPassword.setError("Please enter more than 6 character!");
            return;
        }

        showProgress();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            final FirebaseUser user = mAuth.getCurrentUser();
                            if (user.isEmailVerified()) {
                                LoginViewModel.Factory factory = new LoginViewModel.Factory(getApplication(), user.getUid());
                                loginViewModel = ViewModelProviders.of(LoginActivity.this, factory).get(LoginViewModel.class);
                                observeViewModel(loginViewModel);
                            } else {
                                startActivity(new Intent(LoginActivity.this, VerifyEmailActivity.class));
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, "Email or password went wrong !!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void observeViewModel(LoginViewModel loginViewModel) {
        loginViewModel.getUserModelObservable().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(@Nullable UserModel userModel) {
                if (userModel != null) {
                    System.out.println(password);
                    UserShareData.getInstance(LoginActivity.this).save(userModel);
                    onBackPressed();
                }
            }
        });
    }
}
