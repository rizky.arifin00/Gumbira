package com.gumbira.application.gumbiratour.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityExplorePlaceBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.search.Result;
import com.gumbira.application.gumbiratour.view.adapter.ListExplorePlaceAdapter;
import com.gumbira.application.gumbiratour.viewmodel.PlacesViewModel;

import java.util.ArrayList;
import java.util.List;

public class ExplorePlaceActivity extends AppCompatActivity {
    ListExplorePlaceAdapter adapter;
    private PlacesViewModel placesViewModel;
    private ActivityExplorePlaceBinding binding;
    String category, type, keyword, query, kota;
    private ArrayList<Result> resultList;
    private Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_explore_place);

        b = getIntent().getExtras();
        if (b != null) {
            keyword = b.getString("keyword");
            category = b.getString("category");
            type = b.getString("type");
            kota = b.getString("kota");

            binding.explore2Toolbar.setTitle(category);
            System.out.println(category);
        }

        setSupportActionBar(binding.explore2Toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        placesViewModel = ViewModelProviders.of(this).get(PlacesViewModel.class);
        adapter = new ListExplorePlaceAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.explore2ListExplore2.setLayoutManager(linearLayoutManager);
        binding.explore2ListExplore2.setAdapter(adapter);
        observeViewModel(placesViewModel);

    }

    private void observeViewModel(PlacesViewModel placesViewModel) {
        query = keyword + "+" + kota;
        placesViewModel.setKey(query, type);
        placesViewModel.getProjectListObservable().observe(this, new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> results) {
                if (results != null) {
                    System.out.println(results.size() + "main");
                    adapter.setListPlace(results);
                    resultList = (ArrayList<Result>) results;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_exploreplace, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Maps:
                Intent i = new Intent(ExplorePlaceActivity.this, NearbyPlacesActivity.class);
                i.putExtra("places", resultList);
                i.putExtra("category", category);
                startActivity(i);
                return true;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return false;
    }
}