package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemCountryBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.CountryModel;
import com.gumbira.application.gumbiratour.view.ui.TourPackageActivity;

import java.util.List;

/**
 * Created by rizky on 18/12/17.
 */

public class ListCountryAdapter extends RecyclerView.Adapter<ListCountryAdapter.viewHolder> {
    List<? extends CountryModel> listCountry;
    private Context c;

    public void setListCountry(final List<? extends CountryModel> listCountry) {
        this.listCountry = listCountry;
        System.out.println(listCountry.size());
        notifyDataSetChanged();
    }


    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCountryBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_country, parent, false);
        c = parent.getContext();
        return new ListCountryAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ListCountryAdapter.viewHolder holder, final int position) {
        ItemCountryBinding binding = holder.binding;
        binding.setCountry(listCountry.get(position));
        binding.itemCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, TourPackageActivity.class);
                intent.putExtra("state",listCountry.get(position).getCountry());
                c.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(listCountry != null){
            return listCountry.size();
        }else{
            return 0;
        }
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        private ItemCountryBinding binding;

        public viewHolder(ItemCountryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}