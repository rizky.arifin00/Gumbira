package com.gumbira.application.gumbiratour.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemReviewBinding;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.Review;

import java.util.List;

/**
 * Created by rizky on 07/11/17.
 */

public class ListReviewAdapter extends RecyclerView.Adapter<ListReviewAdapter.BindingHolder> {
    List<? extends Review> list;

    public void setList(final List<? extends Review> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemReviewBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_review, parent, false);

        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemReviewBinding binding = holder.binding;
        binding.setReview(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list != null){
            return list.size();
        }else {
            return 0;
        }
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemReviewBinding binding;

        public BindingHolder(ItemReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}