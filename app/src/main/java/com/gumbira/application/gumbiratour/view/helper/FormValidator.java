package com.gumbira.application.gumbiratour.view.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rizky on 16/11/17.
 */

public class FormValidator {
    public boolean isValidEmail(final String userEmail) {
        Pattern pattern;
        Matcher matcher;
        pattern = android.util.Patterns.EMAIL_ADDRESS;
        matcher = pattern.matcher(userEmail);
        return matcher.matches();
    }
}
