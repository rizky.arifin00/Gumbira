package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemKategoriBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.CategoryModel;
import com.gumbira.application.gumbiratour.view.ui.ExplorePlaceActivity;

import java.util.List;

public class ListKategoriAdapter extends RecyclerView.Adapter<ListKategoriAdapter.MasonryView> {
    List<? extends CategoryModel> listKategori;
    Context c;

    public ListKategoriAdapter(Context c){
        this.c = c;
    }

    public void setListKategori(final List<? extends CategoryModel> listKategori) {
        this.listKategori = listKategori;
        System.out.println(listKategori.size());
        notifyDataSetChanged();
    }


    @Override
    public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemKategoriBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_kategori, parent, false);
        return new MasonryView(binding);
//        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori, parent, false);
//        MasonryView masonryView = new MasonryView(layoutView);
//        return masonryView;
    }

    @Override
    public void onBindViewHolder(MasonryView holder, final int position) {
        final ItemKategoriBinding binding = holder.binding;
        binding.setKategori(listKategori.get(position));
        binding.itemCateogry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(c, ExplorePlaceActivity.class);
                i.putExtra("category", listKategori.get(position).getName_category());
                i.putExtra("keyword", listKategori.get(position).getKeyword());
                i.putExtra("type", listKategori.get(position).getType());
                i.putExtra("kota", listKategori.get(position).getCity());
                System.out.println(listKategori.get(position).getType());
                c.startActivity(i);
            }
        });
//        holder.imageView.setImageResource(imgList[position]);
//        holder.textView.setText(nameList[position]);
    }

    @Override
    public int getItemCount() {
        if(listKategori != null) {
            return listKategori.size();
        }
        else {
            return 0;
        }
//        return nameList.length;
    }

    public static class MasonryView extends RecyclerView.ViewHolder {
        private ItemKategoriBinding binding;
//        ImageView imageView;
//        TextView textView;

        public MasonryView(ItemKategoriBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
