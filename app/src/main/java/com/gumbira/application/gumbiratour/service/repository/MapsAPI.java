package com.gumbira.application.gumbiratour.service.repository;

import com.gumbira.application.gumbiratour.service.model.gmaps.Example;
import com.gumbira.application.gumbiratour.service.model.gmaps.detail.Body;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rizky on 10/10/17.
 */

public interface MapsAPI {
    String photoUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference=";
    String key = "&key=AIzaSyBbzjbXqv3qrWyVm-CL1WS8DEtzqWYyCV8";

    @GET("api/place/nearbysearch/json?sensor=true&rankby=distance" + key)
    Call<Example> getNearbyPlaces(@Query("keyword") String keyword, @Query("location") String location,
                                  @Query("type") String type);

    @GET("api/place/details/json?language=id" + key)
    Call<Body> getDetailPlace(@Query("placeid") String placeid);

    @GET("api/place/textsearch/json?language=id" + key)
    Call<Example> getPlaces(@Query("query") String query, @Query("type") String type);

    @GET("api/place/textsearch/json?language=id" + key)
    Call<Example> getNextPage(@Query("query") String query, @Query("type") String type, @Query("pagetoken") String pagetoken);
}
