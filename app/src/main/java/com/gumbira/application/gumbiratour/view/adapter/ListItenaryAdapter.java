package com.gumbira.application.gumbiratour.view.adapter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.gumbira.application.gumbiratour.service.model.firebase.ItenaryModel;
import java.util.List;

/**
 * Created by rizky on 25/11/17.
 */

public class ListItenaryAdapter extends BaseExpandableListAdapter{

    private List<? extends ItenaryModel> itenaryList;

    public void setTourPackList(final List<? extends ItenaryModel> itenaryList){
        this.itenaryList = itenaryList;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        if (itenaryList != null) {
            return itenaryList.size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (itenaryList != null) {
            return itenaryList.get(groupPosition).getTitle();
        }
        return 0;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (itenaryList != null) {
            return itenaryList.get(groupPosition).getDetail();
        }
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TextView txtView = new TextView(parent.getContext());
        txtView.setText(itenaryList.get(groupPosition).getTitle());
        txtView.setTextColor(Color.BLACK);
        txtView.setPadding(60,30,30,30);
        txtView.setTextSize(16);
        txtView.setBackgroundColor(Color.parseColor("#f2f2f2"));
        return txtView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final TextView txtView = new TextView(parent.getContext());
        txtView.setText(itenaryList.get(groupPosition).getDetail());
        txtView.setPadding(30,15,30,15);
        txtView.setTextSize(14);
        return txtView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
