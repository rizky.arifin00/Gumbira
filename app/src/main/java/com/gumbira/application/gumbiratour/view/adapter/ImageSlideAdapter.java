package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.service.model.gmaps.Photo;

import java.util.List;

/**
 * Created by rizky on 13/10/17.
 */

public class ImageSlideAdapter extends PagerAdapter {
    private List<Photo> photoList;
    private LayoutInflater inflater;
    private Context context;
    private boolean center;

    public ImageSlideAdapter(Context context, List<Photo> photoList, boolean center) {
        this.context = context;
        this.photoList = photoList;
        this.center = center;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if(center){
            return 5;
        }
        return photoList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        System.out.println(photoList.get(position).getPhotoReference());
        if (center) {
            Glide.with(context).load(photoList.get(position).getPhotoReference()).centerCrop().into(myImage);
        } else {
            Glide.with(context).load(photoList.get(position).getPhotoReference()).into(myImage);
        }
        /*Glide.with(context).load(MapsAPI.photoUrl+images.get(position)+MapsAPI.key).placeholder(R.mipmap.ic_launcher).into(myImage);*/
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}