package com.gumbira.application.gumbiratour.service.model.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;

/**
 * Created by ASUS on 10/30/17.
 */

public class Staggered implements AsymmetricItem {
    private int columnSpan;
    private int rowSpan;
    private int position;

    public Staggered() {
        this(1, 1, 0);
    }

    public Staggered(int columnSpan, int rowSpan, int position) {
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
        this.position = position;
    }

    public Staggered(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int getColumnSpan() {
        return columnSpan;
    }

    public void setColumnSpan(int columnSpan) {
        this.columnSpan = columnSpan;
    }

    @Override
    public int getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(int rowSpan) {
        this.rowSpan = rowSpan;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override public String toString() {
        return String.format("%s: %sx%s", position, rowSpan, columnSpan);
    }

    @Override public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
    }

    @Override public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(columnSpan);
        dest.writeInt(rowSpan);
        dest.writeInt(position);
    }

    public static final Parcelable.Creator<Staggered> CREATOR = new Parcelable.Creator<Staggered>() {
        @Override public Staggered createFromParcel(@NonNull Parcel in) {
            return new Staggered(in);
        }

        @Override @NonNull
        public Staggered[] newArray(int size) {
            return new Staggered[size];
        }
    };
}
