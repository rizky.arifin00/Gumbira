package com.gumbira.application.gumbiratour.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.gumbira.application.gumbiratour.service.model.gmaps.detail.ResultDetail;
import com.gumbira.application.gumbiratour.service.repository.MapsRepository;

/**
 * Created by rizky on 25/10/17.
 */

public class DetailPlaceViewModel extends AndroidViewModel{
    private final LiveData<ResultDetail> placeObservable;
    private final String placeid;

    public ObservableField<ResultDetail> place = new ObservableField<>();

    public DetailPlaceViewModel(@NonNull Application application,
                            final String placeid) {
        super(application);
        this.placeid = placeid;

        placeObservable = MapsRepository.getInstance().getDetailPlace(placeid);
    }

    public LiveData<ResultDetail> getPlaceObservable() {
        return placeObservable;
    }

    public void setPlace(ResultDetail place) {
        this.place.set(place);
    }

    /**
     * A creator is used to inject the project ID into the ViewModel
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final String placeid;

        public Factory(@NonNull Application application, String placeid) {
            this.application = application;
            this.placeid = placeid;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
             //noinspection unchecked
            return (T) new DetailPlaceViewModel(application, placeid);
        }
    }
}
