package com.gumbira.application.gumbiratour.view.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivitySignUpBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.gumbira.application.gumbiratour.view.helper.EmailVerifyHelper;
import com.gumbira.application.gumbiratour.view.helper.FormValidator;
import com.gumbira.application.gumbiratour.view.sharedata.UserShareData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignUpActivity extends AppCompatActivity {
    private ActivitySignUpBinding binding;
    private boolean emailUse, success;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mRef = mRootRef.child("user");
    private String username, email, password, vpassword;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        binding.setSignup(this);

        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        binding.toolbarSignup.setTitle("");
        setSupportActionBar(binding.toolbarSignup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        binding.signupBtnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        success = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    private void showProgress() {
        progressDialog.setMessage("Registering user ...  ");
        progressDialog.show();
    }

    private void hideProgress() {
        progressDialog.dismiss();
    }

    private void registerUser() {
        View v = this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        FormValidator formValidator = new FormValidator();
        email = binding.signupEmail.getText().toString().trim();
        username = binding.signupUsername.getText().toString().trim();
        password = binding.signupPassword.getText().toString().trim();
//        vpassword = binding.signupVpassword.getText().toString().trim();

        if (TextUtils.isEmpty(username)) {
            binding.signupUsername.setError("Please enter name!");
            return;
        } else if (username.length() < 3) {
            binding.signupUsername.setError("Please enter more than 3 character!");
            return;
        }
        if (TextUtils.isEmpty(email)) {
            binding.signupEmail.setError("Please enter email!");
            return;
        } else if (!formValidator.isValidEmail(email)) {
            binding.signupEmail.setError("Please enter valid email!");
            return;
        } else if (!TextUtils.isEmpty(email)) {
            checkEmail(email);
            if (emailUse) {
                return;
            }
        }
        if (TextUtils.isEmpty(password)) {
            binding.signupPassword.setError("Please enter password!");
            return;
        } else if (password.length() < 6) {
            binding.signupPassword.setError("Please enter more than 6 character!");
            return;
        }
//        if (TextUtils.isEmpty(vpassword)) {
//            binding.signupVpassword.setError("Please verify password!");
//            return;
//        } else if (!password.equals(vpassword)) {
//            binding.signupVpassword.setError("Password not matched");
//            return;
//        }

        showProgress();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            success = true;
                            user = mAuth.getCurrentUser();
                            String id = user.getUid();
                            UserModel userModelData = new UserModel(id, username, email, "");
                            mRef.child(id).setValue(userModelData);
                            UserShareData.getInstance(SignUpActivity.this).save(userModelData);
                            EmailVerifyHelper emailVerifyHelper = new EmailVerifyHelper();
                            if (emailVerifyHelper.verifyEmail(SignUpActivity.this, user)) {
                                hideProgress();
                            }
                        } else {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }
                });
    }

    private void checkEmail(final String email) {
        mRootRef.child("user").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    UserModel userModel = data.getValue(UserModel.class);
                    if (email.equals(userModel.getEmail())) {
                        if (!success) {
                            binding.signupEmail.setError("Email already used!");
                        }
                        emailUse = true;
                    } else {
                        emailUse = false;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
