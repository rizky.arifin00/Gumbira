package com.gumbira.application.gumbiratour.view.adapter;

/**
 * Created by rizky on 09/11/17.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.AbsListView.LayoutParams;

import com.bumptech.glide.Glide;
import com.gumbira.application.gumbiratour.service.model.gmaps.Photo;

import java.util.List;

public class ImageGalleryAdapter extends BaseAdapter {
    private Context mContext;
    private List<Photo> photoList;

    // Keep all Images in array
//    public Integer[] mThumbIds = {
//            R.drawable.tsb, R.drawable.rancaupas, R.drawable.beach09,
//            R.drawable.banner, R.drawable.papandayan, R.drawable.gedungsate,
//            R.drawable.banner2, R.drawable.tsb, R.drawable.rancaupas,
//            R.drawable.beach09, R.drawable.banner, R.drawable.papandayan,
//            R.drawable.gedungsate, R.drawable.banner2, R.drawable.tsb,
//    };

    // Constructor
    public ImageGalleryAdapter(Context c, List<Photo> photoList){
        this.photoList = photoList;
        mContext = c;
    }

    @Override
    public int getCount() {
        return photoList.size();
    }

    @Override
    public Object getItem(int position) {
        return photoList.get(position).getPhotoReference();
    }

    @Override
    public long getItemId(int position) {
        return 10;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        Glide.with(mContext).load(photoList.get(position).getPhotoReference()).centerCrop().into(imageView);
//        imageView.setImageResource(mThumbIds[position]);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        imageView.setLayoutParams(new GridView.LayoutParams(160, 160));
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,220);
        System.out.println(ViewGroup.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }
}