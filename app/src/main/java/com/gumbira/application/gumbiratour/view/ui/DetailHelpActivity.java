package com.gumbira.application.gumbiratour.view.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ActivityDetailHelpBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.HelpModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailHelpActivity extends AppCompatActivity {
    private Bundle b;
    private String nama;
    private HelpModel helpModel;
    private ActivityDetailHelpBinding binding;

    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mRef = mRootRef.child("helpModel");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_help);

        b = getIntent().getExtras();
        if(b != null){
            nama = b.getString("nama");

            mRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()){
                        helpModel = data.getValue(HelpModel.class);
                        Toast.makeText(getApplicationContext(), helpModel.getKeluhan() , Toast.LENGTH_SHORT).show();
                        if (nama == helpModel.getKeluhan()){
                        binding.tvDetail.setText(helpModel.getDetail());
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
