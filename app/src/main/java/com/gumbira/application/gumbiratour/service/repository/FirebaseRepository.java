package com.gumbira.application.gumbiratour.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.service.model.firebase.UserModel;
import com.gumbira.application.gumbiratour.service.model.firebase.WantogoModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rizky on 14/11/17.
 */

public class FirebaseRepository {
    private static FirebaseRepository firebaseRepository;
    private DatabaseReference mRootRef;

    private FirebaseRepository() {
        mRootRef = FirebaseDatabase.getInstance().getReference();
    }

    public synchronized static FirebaseRepository getInstance() {
        if (firebaseRepository == null) {
            if (firebaseRepository == null) {
                firebaseRepository = new FirebaseRepository();
            }
        }
        return firebaseRepository;
    }

    public LiveData<UserModel> getUserById(final String id) {
        final MutableLiveData<UserModel> data = new MutableLiveData<>();
        mRootRef.child("user").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataChild : dataSnapshot.getChildren()) {
                    UserModel userModel = dataChild.getValue(UserModel.class);
                    if (id.equals(userModel.getUid())) {
                        data.setValue(userModel);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("error ");
            }
        });
        return data;
    }

    public LiveData<TourPackModel> getPackageById(final String packageid, String state) {
        final MutableLiveData<TourPackModel> data = new MutableLiveData<>();
        mRootRef.child("package").child(state).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataChild : dataSnapshot.getChildren()) {
                    TourPackModel tourPackModel = dataChild.getValue(TourPackModel.class);
                    if (packageid.equals(tourPackModel.getId_package())) {
                        data.setValue(tourPackModel);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("error ");
            }
        });
        return data;
    }

    public LiveData<List<TourPackModel>> getTourPackage(String state, final String promo) {
        final MutableLiveData<List<TourPackModel>> data = new MutableLiveData<>();
        mRootRef.child("package").child(state).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<TourPackModel> tourPackModelList = new ArrayList<>();
                for (DataSnapshot dataChild : dataSnapshot.getChildren()) {
                    TourPackModel tourPackModel = dataChild.getValue(TourPackModel.class);
                    if (promo != null) {
                        if (!tourPackModel.getPromo().equals("0% OFF")) {
                            tourPackModelList.add(tourPackModel);
                        }
                    } else {
                        tourPackModelList.add(tourPackModel);
                    }
                }
                data.setValue(tourPackModelList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Error!");
            }
        });
        return data;
    }

    public LiveData<List<WantogoModel>> getWantToGo(String uid) {
        final MutableLiveData<List<WantogoModel>> data = new MutableLiveData<>();
        mRootRef.child("wishlist").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<WantogoModel> wantogoModelList = new ArrayList<>();
                for (DataSnapshot dataChild : dataSnapshot.getChildren()) {
                    WantogoModel wantogoModel = dataChild.getValue(WantogoModel.class);
                    wantogoModelList.add(wantogoModel);
                }
                data.setValue(wantogoModelList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Error!");
            }
        });
        return data;
    }
}
