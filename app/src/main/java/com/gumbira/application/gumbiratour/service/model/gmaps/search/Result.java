package com.gumbira.application.gumbiratour.service.model.gmaps.search;

/**
 * Created by rizky on 10/10/17.
 */

import android.support.annotation.Nullable;

import com.gumbira.application.gumbiratour.service.model.gmaps.Geometry;
import com.gumbira.application.gumbiratour.service.model.gmaps.OpeningHours;
import com.gumbira.application.gumbiratour.service.model.gmaps.Photo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Result implements Serializable {
    @SerializedName("formatted_address")
    @Expose
    private String formattedAddress;
    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("opening_hours")
    @Expose
    private OpeningHours openingHours;
    @SerializedName("photos")
    @Expose
    private List<Photo> photos = new ArrayList<Photo>();
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("types")
    @Expose
    private List<String> types = new ArrayList<String>();
    @SerializedName("vicinity")
    @Expose
    private String vicinity;
    @SerializedName("price_level")
    @Expose
    private Integer priceLevel;

    public Result() {
    }

    public Result(String name, String vicinity) {
        this.name = name;
        this.vicinity = vicinity;
    }

    public @Nullable
    String getImage() {
        if (photos == null || photos.size() == 0) {
            System.out.println("ikan");
            return "";
        } else {
            return photos.get(0).getPhotoReference();
        }
    }

    public String getType() {
        String type = types.get(0);
        type = type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
        type = type.replace("_", " ");
        return type;
    }

    public String getStrRating() {
        if (rating == null) {
            return "0";
        } else {
            return rating.toString();
        }
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return The geometry
     */
    public Geometry getGeometry() {
        return geometry;
    }

    /**
     * @param geometry The geometry
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    /**
     * @return The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The openingHours
     */
    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    /**
     * @param openingHours The opening_hours
     */
    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    /**
     * @return The photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    /**
     * @return The placeId
     */
    public String getPlaceId() {
        return placeId;
    }

    /**
     * @param placeId The place_id
     */
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return The rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     * @param rating The rating
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    /**
     * @return The reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference The reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return The scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * @param scope The scope
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * @return The types
     */
    public List<String> getTypes() {
        return types;
    }

    /**
     * @param types The types
     */
    public void setTypes(List<String> types) {
        this.types = types;
    }

    /**
     * @return The vicinity
     */
    public String getVicinity() {
        return vicinity;
    }

    /**
     * @param vicinity The vicinity
     */
    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    /**
     * @return The priceLevel
     */
    public Integer getPriceLevel() {
        return priceLevel;
    }

    /**
     * @param priceLevel The price_level
     */
    public void setPriceLevel(Integer priceLevel) {
        this.priceLevel = priceLevel;
    }

    public String getAddress(){
        if (vicinity!=null) {
            if (vicinity.length() > 70) {
                return vicinity.substring(0, 70) + "...";
            } else {
                return vicinity;
            }
        }
        else {
            if (formattedAddress.length() > 70) {
                return formattedAddress.substring(0, 70) + "...";
            } else {
                return formattedAddress;
            }
        }
    }

    public String getLongAdress(){
        if (vicinity!=null) {
            if (vicinity.length() > 85) {
                return vicinity.substring(0, 85) + "...";
            } else {
                return vicinity;
            }
        }
        else {
            if (formattedAddress.length() > 85) {
                return formattedAddress.substring(0, 85) + "...";
            } else {
                return formattedAddress;
            }
        }
    }
}
