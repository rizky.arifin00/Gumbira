package com.gumbira.application.gumbiratour.view.sharedata;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rizky on 01/12/17.
 */

public class CityShareData {
    private static CityShareData cityShareData;
    private static final String PREFS_NAME = "City";
    private static final String CITYDATA_KEY = "citydata";
    private static final String COUNTRY_KEY = "countrydata";
    private static final String ADMIN_AREA_KEY = "adminareadata";
    private static final String ADDRESS_KEY = "addressdata";
    private SharedPreferences.Editor editor;
    private SharedPreferences settings;

    private CityShareData(Context context){
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    public synchronized static CityShareData getInstance(Context context){
        if (cityShareData == null){
            if (cityShareData == null) {
                cityShareData = new CityShareData(context);
            }
        }
        return cityShareData;
    }

    public void saveAddress(String address){
        editor.putString(ADDRESS_KEY, address);
        editor.commit();
    }

    public void saveCity(String city){
        editor.putString(CITYDATA_KEY, city);
        editor.commit();
    }

    public void saveCountry(String country){
        editor.putString(COUNTRY_KEY, country);
        editor.commit();
    }

    public void saveAdminArea(String adminarea){
        editor.putString(ADMIN_AREA_KEY, adminarea);
        editor.commit();
    }

    public String getCity(){
        return settings.getString(CITYDATA_KEY, "");
    }

    public String getCountry(){
        return settings.getString(COUNTRY_KEY, "");
    }

    public String getAdminArea(){
        return settings.getString(ADMIN_AREA_KEY, "");
    }

    public String getAddress(){
        return settings.getString(ADDRESS_KEY, "");
    }

    public void clear(){
        editor.remove(PREFS_NAME);
        editor.commit();
    }
}
