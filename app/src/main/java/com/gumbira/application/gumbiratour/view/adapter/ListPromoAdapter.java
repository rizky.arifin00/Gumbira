package com.gumbira.application.gumbiratour.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gumbira.application.gumbiratour.R;
import com.gumbira.application.gumbiratour.databinding.ItemPromoBinding;
import com.gumbira.application.gumbiratour.service.model.firebase.TourPackModel;
import com.gumbira.application.gumbiratour.view.ui.DetailPackageActivity;

import java.util.List;

/**
 * Created by rizky on 03/11/17.
 */

public class ListPromoAdapter extends RecyclerView.Adapter<ListPromoAdapter.viewHolder> {
    List<? extends TourPackModel> promolist;
    private Context c;

    public void setPromolist(final List<? extends TourPackModel> promolist) {
        this.promolist = promolist;
        notifyDataSetChanged();
    }

    @Override
    public ListPromoAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPromoBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_promo, parent, false);
        c = parent.getContext();
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {
        ItemPromoBinding binding = holder.binding;
        if (promolist != null) {
            binding.setListpromo(promolist.get(position));
            binding.imagePackage.setLabelText(promolist.get(position).getPromo());
            binding.itemPromo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(c, DetailPackageActivity.class);
                    i.putExtra("id", promolist.get(position).getId_package());
                    i.putExtra("state", promolist.get(position).getState());
                    c.startActivity(i);
                }
            });
            binding.tvHargaOriginal.setPaintFlags(binding.tvHargaOriginal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    @Override
    public int getItemCount() {
        if (promolist != null){
            return promolist.size();}
        else {
            return 0;
        }
    }

    public static class viewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ItemPromoBinding binding;

        public viewHolder(ItemPromoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}